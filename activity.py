from model import OObject
from model.schedule import Schedule

def _conflicts(schedule, other):
    '''
    >>> _conflicts(Schedule(day="monday"), Schedule(day="tuesday"))
    False
    >>> _conflicts(Schedule(day="monday", beginning=8, end=10), Schedule(day="monday", beginning=8, end=11))
    True
    >>> _conflicts(Schedule(day="monday", beginning=8, end=10), Schedule(day="monday", beginning=10, end=12))
    False
    >>> _conflicts(Schedule(day="monday", beginning=8, end=11), Schedule(day="monday", beginning=9, end=10))
    True
    >>> _conflicts(Schedule(day="monday", beginning=8, end=11), Schedule(day="monday", beginning=14, end=16))
    False
    '''
    if not schedule.day == other.day:
        return False
    elif schedule.end <= other.beginning:
        return False
    elif schedule.end <= other.beginning:
        return False
    return True

class Activity(OObject):

    def __init__(self, cycle=None, classe=None, teacher=None, installation=None, sport=None, schedule=None):
        self.id = "{}/{}/{}/{}/{}/{}".format(repr(cycle), repr(classe), repr(teacher), repr(installation), repr(sport), repr(schedule))
        self.cycle = cycle
        self.classe = classe
        self.teacher = teacher
        self.installation = installation
        self.sport = sport
        self.schedule = schedule

    def conflicts(self, other):
        if any(period in other.cycle.periods for period in self.cycle.periods):
            res = _conflicts(self.schedule, other.schedule)
        else:
            res = False
        return res