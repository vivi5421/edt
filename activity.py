import itertools
from model import OObject

class Activity(OObject):
    def __init__(self, cycle=None, classe=None, teacher=None, installation=None, sport=None, schedule=None):
        self.id = "{}/{}/{}/{}/{}/{}".format(repr(cycle), repr(classe), repr(teacher), repr(installation), repr(sport), repr(schedule))
        self.cycle = cycle
        self.classe = classe
        self.teacher = teacher
        self.installation = installation
        self.sport = sport
        self.schedule = schedule

class Activities(list):

    def format_output(self, llist, duplicates):
        return Activities(llist) if duplicates else Activities(list(set(llist)))

    def get_cycles(self, classe=None, teacher=None, cycle=None, duplicates=False):
        llist = [act.cycle for act in self.get_activities(classe, teacher, cycle)]
        return self.format_output(llist, duplicates)

    def get_classes(self, classe=None, teacher=None, cycle=None, duplicates=False):
        llist = [act.classe for act in self.get_activities(classe, teacher, cycle)]
        return self.format_output(llist, duplicates)
    
    def get_sports(self, classe=None, teacher=None, cycle=None, duplicates=False):
        llist = [act.sport for act in self.get_activities(classe, teacher, cycle)]
        return self.format_output(llist, duplicates)
    
    def get_schedules(self, classe=None, teacher=None, cycle=None, duplicates=False):
        llist = [act.schedule for act in self.get_activities(classe, teacher, cycle)]
        return self.format_output(llist, duplicates)

    def get_teachers(self, classe=None, teacher=None, cycle=None, duplicates=False):
        llist = [act.teacher for act in self.get_activities(classe, teacher, cycle)]
        return self.format_output(llist, duplicates)

    def get_activities(self, classe=None, teacher=None, cycle=None):
        result = self
        if classe:
            result = [act for act in result if act.classe == classe]
        if teacher:
            result = [act for act in result if act.teacher == teacher]
        if cycle:
            result = [act for act in result if act.cycle == cycle]
        return self.format_output(result, False)
    
    def _conflicts(self):
        for _, same_day in itertools.groupby(sorted(list(self), key=lambda act: act.schedule.day), key=lambda act: act.schedule.day):
            same_day_acts = list(same_day)
            if len(same_day_acts) > 1:
                if any([set(range(left.schedule.beginning, left.schedule.end)).intersection(range(right.schedule.beginning, right.schedule.end)) for left in same_day_acts for right in same_day_acts if not left == right]):
                    return True
        return False

    def conflicts(self, consider_cycles=True):
        """
        >>> m_10_12 = Activity(schedule=Schedule("Monday", 10, 12), cycle=Cycle(1))
        >>> m_8_11 = Activity(schedule=Schedule("Monday", 8, 11), cycle=Cycle(1))
        >>> m_12_14 = Activity(schedule=Schedule("Monday", 12, 14), cycle=Cycle(1))
        >>> tu_10_12 = Activity(schedule=Schedule("Tuesday", 10, 12), cycle=Cycle(1))
        >>> Act.conflicts([m_10_12, m_8_11]) 
        True
        >>> Act.conflicts([m_10_12, m_12_14])
        False
        >>> Act.conflicts([m_10_12, tu_10_12])
        False
        """
        if consider_cycles:
            result = True
            for _, same_cycle in itertools.groupby(sorted(self, key=lambda act: act.cycle.id), key=lambda act: act.cycle.id):
                result &= Activities(same_cycle)._conflicts()
            return result    
        else:
            return self._conflicts()
    
    def __add__(self, other):
        tl = Activities(self)
        [tl.append(oth) for oth in other]
        return tl