import argparse
import jsonpickle
import logging

from os import mkdir
from os.path import join
from shutil import rmtree
from sys import stdout

LOGS = {
    "d": logging.DEBUG,
    "i": logging.INFO,
    "w": logging.WARNING,
    "e": logging.ERROR,
    "c": logging.CRITICAL
}

def load_config(config):
    with open(config, "r") as f:
        iinput = "".join(f.readlines())
    configuration = jsonpickle.decode(iinput)
    configuration.sanity()
    configuration.forward_indexes()
    return configuration

def get_options(args):
    parser = argparse.ArgumentParser(description="Generate schedules for EPS teachers.")
    parser.add_argument("--log", help="Level of logs", type=lambda x: LOGS[x], default=logging.INFO)
    parser.add_argument("--display-prefilters", dest="display_prefilters", help="Display prefiltering output. Requires log=d", action="store_true")
    parser.add_argument("--valids", help="Highlight valids permutations during processing", action="store_true")
    parser.add_argument("--steps", help="Process step-by-step", action="store_true")
    parser.add_argument("--steps-when-valid", dest="steps_when_valid", help="Do the break only in the case you have a not blocking scenario", action="store_true")
    parser.add_argument("--profiling", help="Activate the profiling and provide the path file", action="store_true")
    parser.add_argument("--save-delay", dest="save_delay", help="Save status every x seconds. 0 to do not save.", default=3600, type=int)
    parser.add_argument("--restart", help="Restart a new full parsing", action="store_true")
    parser.add_argument("--valids-file", dest="valids_file", help=argparse.SUPPRESS, default="valids.out")
    parser.add_argument("--checkpoint-file", dest="checkpoint_file", help=argparse.SUPPRESS, default=".checkpoint")
    parser.add_argument("--recursion-limit", dest="recursion_limit", help=argparse.SUPPRESS, type=int, default=10000)
    parser.add_argument("--progress-bar", dest="progress_bar", help="Refresh progressbar every x seconds. 0 for at each step. -1 for never", type=int, default=10)
    parser.add_argument("--is_save_required", help=argparse.SUPPRESS, action="store_true")
    parser.add_argument("--is_process_finished", help=argparse.SUPPRESS, action="store_true")
    parser.add_argument("--sort_filters", help=argparse.SUPPRESS, default=lambda x: x.priority)
    parser.add_argument("--test_setup", help="Prepare for testing to be able to check some indices configurations.", action="store_true")
    parser.add_argument("config", help=argparse.SUPPRESS, type=load_config)
    return parser.parse_args(args)