from filters import Filter

class HasAllClasses(Filter):

    priority = 1000

    def filter(self, activities, groups):
        return len(set(x.classe.id for x in activities)) == self.nb_classes
