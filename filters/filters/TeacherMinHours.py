from filters import Filter
from groups import SORT_TEACHER_CYCLE

class TeacherMinHours(Filter):

    def filter(self, activities, groups):
        for teacher_cycle, activities in groups.get(SORT_TEACHER_CYCLE):
            if sum(x.schedule.length() for x in activities) < teacher_cycle[0].min_hours:
                return False
        return True
