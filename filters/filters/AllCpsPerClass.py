from filters import Filter
from utils import group
from groups import SORT_CLASSE

class AllCpsPerClass(Filter):

    def filter(self, activities, groups):
        for _, classe_sports in groups.get(SORT_CLASSE):
            if not len(set(act.sport.cp for act in list(classe_sports))) == self.nb_cps:
                return False
        return True
