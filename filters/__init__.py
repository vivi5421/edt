import logging
import configuration
from model.teacher import Teacher
from model.cycle import Cycle
from model.classe_type import ClasseType
from model.classe import Classe
from model.installation import Installation
from model.schedule import Schedule
from model.sport import Sport
import traceback


logger = logging.getLogger("edt")
_sort_lambda = lambda x: x.priority

class Filter(object):

    priority = 100

    def __init__(self, args):
        self.args = args
        try:
            self.config = args.config
            # Variables to be calculated before
            self.nb_classes = len(self.config.classes)
            self.cps = set(sport.cp for sport in self.config.sports)
            self.nb_cps = len(self.cps)
            self.cycles_per_classe = {classe.id: len(classe.type.cycles) for classe in self.config.classes}
        except AttributeError:
            logger.error(traceback.format_exc())
            pass
    
    def filter(self, activity):
        '''
        if is_filter: activities refers to a set of activities making a full schedule proposal.
        if is_prefilter: activities refers the the list of all available activities. Returns the filtered list of activities.
        '''
        raise NotImplementedError("filter(self, activities) method not implemented for class {}".format(self.__class__))

    def filter(self, activities, groups):
        '''
        if is_filter: activities refers to a set of activities making a full schedule proposal.
        if is_prefilter: activities refers the the list of all available activities. Returns the filtered list of activities.
        '''
        raise NotImplementedError("filter(self, activities) method not implemented for class {}".format(self.__class__))

    def __str__(self):
        return self.__class__.__name__
    
    def __repr__(self):
        return self.__class__.__name__
