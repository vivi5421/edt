from filters import Filter
from utils import group
from groups import SORT_TEACHER_CYCLE_SCHEDULE_DAY

class Max6HoursPerDay(Filter):

    def filter(self, activities, groups):
        for _, v in groups.get(SORT_TEACHER_CYCLE_SCHEDULE_DAY):
            if sum(x.schedule.length() for x in v) > 6:
                return False
        return True
