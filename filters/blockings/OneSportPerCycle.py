from filters import Filter
from utils import group
from groups import SORT_CLASSE_CYCLE

class OneSportPerCycle(Filter):

    def filter(self, activities, groups):
        for _, v in groups.get(SORT_CLASSE_CYCLE):
            if len(list(v)) > 1:
                return False
        return True
