from filters import Filter
from utils import group
from groups import SORT_INSTALLATION

class NoConflictInstallations(Filter):

    def filter(self, activities, groups):
        # To be checked if better with sort_installation_period1, sort_installation_period2...
        for key, value in groups.get(SORT_INSTALLATION):
            periods = [[], [], [], []]
            for item in value:
                for period in item.cycle.periods:
                    periods[period - 1].append(item)
            for period in periods:
                for i in range(len(period)):
                    if period[i].classe.type.size + sum(period[j].classe.type.size for j in range(i + 1, len(period)) if period[i].conflicts(period[j])) > key.capacity:
                        return False
        return True    
