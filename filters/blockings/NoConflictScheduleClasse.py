from filters import Filter
from itertools import combinations
from utils import group
from groups import SORT_CLASSE

class NoConflictScheduleClasse(Filter):

    def filter(self, activities, groups):
        for _, classe_cycle_schedules in groups.get(SORT_CLASSE):
            for i, j in combinations(classe_cycle_schedules, 2):
                if i.conflicts(j):
                    return False
        return True
