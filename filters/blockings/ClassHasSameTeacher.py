from filters import Filter
from utils import group
from groups import SORT_CLASSE

class ClassHasSameTeacher(Filter):

    priority = 1000

    def filter(self, activities, ggroups):
        for _, classe_activities in ggroups.get(SORT_CLASSE):
            if not len(set(act.teacher for act in classe_activities)) == 1:
                return False
        return True