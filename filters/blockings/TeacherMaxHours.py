from filters import Filter
from utils import group
from groups import SORT_TEACHER_CYCLE

class TeacherMaxHours(Filter):

    def filter(self, activities, groups):
        for teacher_cycle, activities in groups.get(SORT_TEACHER_CYCLE):
            if sum(x.schedule.length() for x in activities) > teacher_cycle[0].max_hours:
                return False
        return True
