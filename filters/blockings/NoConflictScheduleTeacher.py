from filters import Filter
from itertools import combinations
from utils import group
from groups import SORT_TEACHER

class NoConflictScheduleTeacher(Filter):

    def filter(self, activities, groups):
        for _, teacher_cycle_schedules in groups.get(SORT_TEACHER):
            for combs in combinations(teacher_cycle_schedules, 2):
                if combs[0].conflicts(combs[1]):
                    return False
        return True
