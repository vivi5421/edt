from filters import Filter
from itertools import combinations
from model.schedule import Schedule
from groups import SORT_CLASSE_CYCLE


def hours_between(schedule1, schedule2):
    '''
    >>> hours_between(Schedule(None, "tuesday", 10, 12), Schedule(None, "tuesday", 14, 17))
    2
    >>> hours_between(Schedule(None, "tuesday", 14, 17), Schedule(None, "tuesday", 10, 12))
    2
    >>> hours_between(Schedule(None, "wednesday", 14, 17), Schedule(None, "tuesday", 10, 12))
    26
    >>> hours_between(Schedule(None, "monday", 8, 10), Schedule(None, "monday", 10, 12))
    0
    >>> hours_between(Schedule(None, "monday", 8, 10), Schedule(None, "monday", 8, 10))
    -2
    '''
    llist = [schedule1, schedule2]
    mmax = max(llist)
    mmin = min(llist)
    res = mmax.time() - mmin.time(end=True)
    return res

class Min24HoursBetweenSessions(Filter):

    def filter(self, activities, groups):
        for _, classe_activities in groups.get(SORT_CLASSE_CYCLE):
            for i, j in combinations(classe_activities, 2):
                time_between = hours_between(i.schedule, j.schedule)
                if not time_between >= 24:
                    return False
        return True 
