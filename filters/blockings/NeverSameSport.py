from filters import Filter
from utils import group
from groups import SORT_CLASSE

class NeverSameSport(Filter):

    def filter(self, activities, groups):
        for _, activities in groups.get(SORT_CLASSE):
            vv = list([activity.sport for activity in activities])
            if not len(set(vv)) == len(vv):
                return False
        return True
