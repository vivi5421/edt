from filters import Filter
from utils import group
from groups import SORT_CLASSE

class SameClassSameSchedule(Filter):

    def filter(self, activities, groups):
        for _, classe_activities in groups.get(SORT_CLASSE):
            if len(set([x.schedule for x in classe_activities])) > 1:
                return False
        return True
