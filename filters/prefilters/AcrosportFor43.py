from filters import Filter


class AcrosportFor43(Filter):

    def filter(self, activity):
        return not activity.sport.id == "acrosport" or activity.classe.level in [4, 3]
