from filters import Filter


def is_valid(activity, teacher):
    return not activity.teacher.id == teacher or not activity.schedule.day == "wednesday"


class NoActivityWednesdayBaptiste(Filter):

    def filter(self, activity):
        return is_valid(activity, "baptiste")


class NoActivityWednesdayTony(Filter):

    def filter(self, activity):
        return is_valid(activity, "tony")
