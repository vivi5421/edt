from filters import Filter


class ThreeHoursSessions(Filter):

    def filter(self, activity):
        return activity.installation.length == activity.classe.type.length
