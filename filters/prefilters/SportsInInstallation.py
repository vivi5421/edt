from filters import Filter


class SportsInInstallation(Filter):

    def filter(self, activity):
        return activity.sport in activity.installation.sports
