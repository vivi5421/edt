from filters import Filter


class COFor43NotCham(Filter):

    def filter(self, activity):
        if activity.sport.id == "course d'orientation":
            if activity.classe.level <= 4 and activity.schedule.length() == 3:
                return True
            return False
        return True
