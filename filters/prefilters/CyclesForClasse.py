from filters import Filter


class CyclesForClasse(Filter):

    def filter(self, activity):
        return activity.cycle in activity.classe.type.cycles
