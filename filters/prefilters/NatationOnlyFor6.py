from filters import Filter


class NatationOnlyFor6(Filter):

    def filter(self, activity):
        if activity.sport.id == "natation" and activity.classe.level <= 5:
            return False
        return True
