from filters import Filter

class Prog_2019(Filter):

    def filter(self, activity):

        # Les sixiemes...
        if activity.classe.level == 6:

            # 6emes / Cham classe entiere
            if activity.classe.type.has_cham and activity.classe.type.cham:
                return activity.sport.id in ["basket", "badminton", "gymnastique", "natation"]
            # 6emes / Cham partie non-cham
            elif activity.classe.type.has_cham:
                return activity.sport.id in ["demi-fond", "athletisme", "danse", "escalade"]
            # 6eme / Non cham
            else:
                return activity.sport.id in ["basket", "badminton", "gymnastique", "natation", "demi-fond", "athletisme", "danse", "escalade"]

        # Les cinquiemes...
        elif activity.classe.level == 5:

            # 5emes / Cham classe entiere
            if activity.classe.type.has_cham and activity.classe.type.cham:
                return activity.sport.id in ["escalade", "gymnastique", "badminton", "demi-fond"]
            # 5emes / Cham partie non-cham
            elif activity.classe.type.has_cham:
                return activity.sport.id in ["escalade", "gymnastique", "badminton", "demi-fond", "volley", "danse"]
            # 5eme / Non cham
            else:
                return activity.sport.id in ["escalade", "gymnastique", "badminton", "demi-fond", "basket", "danse"]

        # Les quatriemes et les troisiemes
        else:

            # 4emes+3emes / Cham classe entiere
            if activity.classe.type.has_cham and activity.classe.type.cham:
                return activity.sport.id in ["acrosport", "escalade", "badminton", "demi-fond"]
            # 4emes+3emes / Cham partie non-cham
            elif activity.classe.type.has_cham:
                return activity.sport.id in ["acrosport", "escalade", "badminton", "demi-fond", "basket", "volley"]
            # 4emes+3emes / Non cham
            else:
                return activity.sport.id in ["escalade", "badminton", "course d'orientation", "athletisme"]

MORNING = lambda x: x.schedule.end <= 13
AFTERNOON = lambda x: x.schedule.beginning >= 13

class Conservatoire(Filter):

    class ConservatoireException(Exception):
        pass
    
    def conservatoire(self, activity, levels, indexes, day, period):
        if activity.classe.level in levels and activity.classe.index in indexes:
            if activity.schedule.day == day and period(activity):
                if activity.classe.type.cham:
                    raise self.ConservatoireException()
            else:
                if not activity.classe.type.cham:
                    raise self.ConservatoireException()


    def filter(self, activity):
        if activity.classe.index in [1, 2] and activity.classe.type.cham:
            if not activity.schedule.day == "monday":
                return False
        
        try:
            self.conservatoire(activity, [6, 5, 4, 3], [3, 4], "monday", AFTERNOON)
            self.conservatoire(activity, [6, 5, 4, 3], [1, 2], "tuesday", AFTERNOON)
            self.conservatoire(activity, [6, 5], [1, 2], "thursday", MORNING)
            self.conservatoire(activity, [4, 3], [1, 2], "thursday", AFTERNOON)
            self.conservatoire(activity, [6, 5], [3, 4], "friday", MORNING)
            self.conservatoire(activity, [4, 3], [3, 4], "friday", AFTERNOON)
            return True
        except self.ConservatoireException:
            return False
    
class NoPiscineCycle4(Filter):

    def filter(self, activity):
        return not activity.sport.id == "natation" or not 4 in activity.cycle.periods

class PiscineAfternoon(Filter):

    def filter(self, activity):
        if activity.sport.id == "natation" and activity.schedule.end >= 12:
            return False
        return True

#service demi-pension de 11h a 14h pas de plateau d
# rotation bus: 60 de sept a dec, 85 de janvier a juin 