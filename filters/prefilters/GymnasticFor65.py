from filters import Filter


class GymnasticFor65(Filter):

    def filter(self, activity):
        return not activity.sport.id == "gymnastique" or activity.classe.level >= 5
