from filters import Filter


class SportsNotForCham(Filter):

    def filter(self, activity):
        if activity.classe.type.cham:
            if activity.sport.id not in ["basket", "volley", "ultimate"]:
                return True
            return False
        return True
