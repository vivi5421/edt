# source: https://stackoverflow.com/questions/40150821/in-the-logging-modules-rotatingfilehandler-how-to-set-the-backupcount-to-a-pra
import logging
from logging.handlers import RotatingFileHandler
from itertools import count
from colorlog import ColoredFormatter
from shutil import rmtree
from os import mkdir
from sys import stdout
from os.path import join, exists

logger = logging.getLogger("edt")

class RollingFileHandler(RotatingFileHandler):
    # override
    def doRollover(self):
        if self.stream:
            self.stream.close()
            self.stream = None
        # my code starts here
        for i in count(1):
            nextName = "%s.%d" % (self.baseFilename, i)
            if not exists(nextName):
                self.rotate(self.baseFilename, nextName)
                break
        # my code ends here
        if not self.delay:
            self.stream = self._open()

def set_logger(args):
    rmtree("logs", ignore_errors=True)
    mkdir("logs")
    try:
        logger.setLevel(args.log)
    except KeyError:
        logger.setLevel(logging.INFO)
        logger.error("Invalid argument for --log {}. Fallback to default".format(key))
    formatter = ColoredFormatter("%(log_color)s%(asctime)s :: %(levelname)s :: %(message)s%(reset)s")
    stdout_handler = logging.StreamHandler(stdout)
    stdout_handler.setLevel(logging.DEBUG)
    stdout_handler.setFormatter(formatter)
    logger.addHandler(stdout_handler)
    file_handler = RollingFileHandler(join("logs", "edt.log"), "w", maxBytes=100*1024*1024)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    return logger