import progress
import time

def test():
    with progress.Progress("toto", 200, opti_cpu=True) as p:
        for i in range(200):
            #print("i={}".format(i))
            time.sleep(1)
            p.next()
        p.finish()

test() 