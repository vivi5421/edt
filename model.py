import gc
import logging

logger = logging.getLogger("edt")

class OObject(object):

    def __init__(self):
        self.id = "<not defined>"
    
    def __str__(self):
        return "<{}: {}>".format(self.__class__.__name__, self.__dict__)
    
    def __repr__(self):
        return "<{}: {}>".format(self.__class__.__name__, self.id)
    
    def __eq__(self, other): 
        return self.__dict__ == other.__dict__
    
    def __hash__(self):
        return hash(self.id)
    
    @classmethod
    def get(cls, key="<all>"):
        objs = [obj for obj in gc.get_objects() if isinstance(obj, cls)]
        if key == "<all>":
            return objs
        else:
            try:
                return [obj for obj in objs if obj.id == key][0]
            except IndexError as e:
                logger.error("Cannot get item [{}] from list [{}]".format(key, objs))
                logger.error(e)

class Schedule(OObject):
    def __init__(self, day=None, beginning=None, end=None):
        self.id = "{} {}-{}".format(day, beginning, end)
        self.day = day
        self.beginning = beginning
        self.end = end
    
    def elapsed_time(self, end=False):
        days = {
            "Monday": 0,
            "Tuesday": 1,
            "Wednesday": 2,
            "Thursday": 3,
            "Friday": 4
        }
        return days[self.day] * 24 + (self.end if end else self.beginning)
    
    def length(self):
        return self.end - self.beginning

class Cycle(OObject):
    def __init__(self, nb=None, extended=False):
        self.id = "{}{}".format(nb, "_{}".format(nb + 1) if extended else "")
        self.extended = extended

class Sport(OObject):
    def __init__(self, name=None, cp=None, shared=False):
        self.id = name
        self.name = name
        self.cp = cp

class Installation(OObject):
    def __init__(self, name=None, _sports=None, bus_required=False, length=None, nb_classes=None):
        self.id = name
        self.name = name
        self._sports = _sports
        self.bus_required = bus_required
        self.length = length
        self.nb_classes = nb_classes

    @property
    def sports(self):
        return [Sport.get(iid) for iid in self._sports]
    
class Classe(OObject):
    def __init__(self, level=None, index=None, hours=None, sessions_per_week=None, _cycles=None, cham=False):
        self.id = "{}*{}{}".format(level, index, "_cham" if cham else "")
        self.level = level
        self.index = index
        self.hours = hours
        self.sessions_per_week = sessions_per_week
        self._cycles = _cycles
        self.cham = cham

    @property
    def cycles(self):
        return [Cycle.get(iid) for iid in self._cycles]

class Teacher(OObject):
    def __init__(self, firstname=None, lastname=None, min_hours=None, max_hours=None):
        self.id = lastname
        self.firstname = firstname
        self.lastname = lastname
        self.min_hours = min_hours
        self.max_hours = max_hours