import locale
import os

from contextlib import contextmanager
from itertools import groupby
from prettytable import PrettyTable
from re import match
from subprocess import check_output

locale.setlocale(locale.LC_ALL, '')

#####################
# GROUPBY FUNCTIONS #
#####################
def group(iterable, key):
    return groupby(sorted(iterable, key=key), key)

##############
# SUBPROCESS #
##############
class Run(object):

    @classmethod
    def run(cls, cmd, print_cmd=True, print_output=True):
        if print_cmd:
            print(cmd)
        output = check_output(cmd, shell=True)
        if print_output:
            print(output)
        return output

#######
# GIT #
#######
class Git(object):

    @classmethod
    def git_clone(cls, repo, branch="master", commit=None, tag=None, recursive=False):
        Run.run("git clone -q -b {branch} {isRecursive} {repo}".format(isRecursive="--recursive" if recursive else "", **locals()))
        if commit:
            with cd(match(".+\/(?P<repo_name>[^\/]+)\.git", repo).group("repo_name")):
                Run.run("git checkout -q {commit}".format(**locals()))

######
# CD #
######
@contextmanager
def cd(path):
    old = os.getcwd()
    try:
        os.chdir(path)
        yield
    finally:
        os.chdir(old)


#############
# PROFILING #
#############
class Profiling(object):

    @classmethod
    def translate(cls, filename):
        import pstats
        p = pstats.Stats(filename)
        p.strip_dirs().sort_stats("cumulative").print_stats()
            

############
# PRINTING #
############
def str_list(llist, rank=False):
    if llist:
        ptable = PrettyTable()
        attrs = [key for key in llist[0].__dict__.keys() if not key == "id"]
        ptable.field_names = ["n"] + attrs
        prefix = 1 if rank else 0
        for item in llist:
            ptable.add_row([prefix + llist.index(item)] + [repr(item.__dict__[attr]) for attr in attrs])
        return ptable
    else:
        return llist

def str_dict(ddict):
    ptable = PrettyTable()
    ptable.field_names = ["key", "value"]
    for k, v in ddict.items():
        ptable.add_row([k, v])
    return ptable

def sci(number, precision=3, sep="e"):
    '''
    >>> sci(1234567891011, precision=6)
    '1.234567e12'
    >>> sci(123456789)
    '1.234e8'
    '''
    try:
        str_num = str(number)
        if len(str_num) > 7:
            return "{}.{}{}{}".format(str_num[0], str_num[1:precision + 1], sep, len(str_num)-1)
        return "{:n}".format(number)
    except TypeError:
        print("Cannot format {}".format(number))

###############
# CALCULATING #
###############
def binom(k, n):
    '''
    $ scipy.special.binom(231000, 270)
    inf

    $ scipy.special.comb(231000, 270)
    inf

    $ scipy.special.comb(231000, 270, exact=True)
    19200894473258607163764799041375967013641114109689411744902525504976027885149640130180408398223623482517337914863581012648806828293510021117025756601209916518702059898778811131491943144720127263386615055403195610024860364202991416778990426874199734250437946664978959246677717781532492765895392812728569908215165883002277071959470896834593840620516756498006576725308012175967146719526445237273706364532332280525506751727549655549294898895985347132784121704317227501173374877829606451008223930710458269443612454435301281953704920459951544503553883798630314604108258770575792056351425305129254851211948708925099076157285400390787602983430260506526922174544693802228907200009393525613631891164022224810162460869804891865365923329101082544584091798192482737895285393168631273700112270719125612924903044203154797329098758361993405852508628324849530758870139773429081087912087255827405730996482272862434464681493200

    $ scipy.special.binom(231000, 270, exact=True)
    Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    TypeError: 'exact' is an invalid keyword to ufunc 'binom'

    def dec_to_sci(x):
        index = x.index(".")
        return "{}.{}E{}".format(x[0], "".join(x[1:5]), )
    '''
    import scipy.special
    return scipy.special.comb(n, k, exact=True)