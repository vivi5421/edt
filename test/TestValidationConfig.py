import edt
import logging
from os.path import join

recurse = None
args = None

def setup_module():
    params = [
        join("configs", "validation.json"),
        "--steps",
        "--save-delay", "0",
        "--restart",
        "--progress-bar", "-1",
        "--test_setup",
        "--log", "e",
    ]
    global recurse
    global args
    recurse, args = edt.edt(params)
    logger = logging.getLogger("edt")
    logger.setLevel(logging.DEBUG)

def test_1_sucess():
    # do not know the expected output
    assert recurse(args=args, indices=[0, 162, 324, 650, 812, 974, 1300, 1462, 1624, 1950, 2142, 2274])