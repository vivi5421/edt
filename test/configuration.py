import logging

from configuration import Configuration
from model.classe import Classe
from model.classe_type import ClasseType
from model.cycle import Cycle
from model.installation import Installation
from model.schedule import Schedule
from model.sport import Sport
from model.teacher import Teacher

class TestConfiguration(Configuration):

    def cycle(self, id, periods, label):
        new = Cycle(id=id, periods=periods, label=label)
        self.cycles.append(new)
        return new

    def classeType(self, id, cycles, cham, has_cham, length, size):
        new = ClasseType(id=id, _cycles=[x.id for x in cycles], cham=cham, has_cham=has_cham, length=length, size=size)
        self.classeTypes.append(new)
        return new

    def classe(self, id, label, level, index, type):
        new = Classe(id=id, label=label, level=level, index=index, _type=type.id)
        self.classes.append(new)
        return new

    def teacher(self, id, label, min_hours, max_hours):
        new = Teacher(id=id, label=label, min_hours=min_hours, max_hours=max_hours)
        self.teachers.append(new)
        return new

    def schedule(self, label, day, beginning, end):
        new = Schedule(label=label, day=day, beginning=beginning, end=end)
        self.schedules.append(new)
        return new

    def sport(self, id, label, name, cp, shared):
        new = Sport(id=id, label=label, name=name, cp=cp, shared=shared)
        self.sports.append(new)
        return new

    def installation(self, id, label, sports, bus_required, length, capacity):
        new = Installation(id=id, label=label, _sports=[x.id for x in sports], bus_required=bus_required, length=length, capacity=capacity)
        self.installations.append(new)
        return new

    def __init__(self):
        self.cycles = []
        self.CYCLE_1 = self.cycle(id="cycle_id_1", periods=[1], label="cycle_label_1")
        self.CYCLE_2 = self.cycle(id="cycle_id_2", periods=[2], label="cycle_label_2")
        self.CYCLE_3 = self.cycle(id="cycle_id_3", periods=[3], label="cycle_label_3")
        self.CYCLE_4 = self.cycle(id="cycle_id_4", periods=[4], label="cycle_label_4")
        self.CYCLE_1_2 = self.cycle(id="cycle_id_1_2", periods=[1, 2], label="cycle_label_1_2")

        self.classeTypes = []
        self.CLASSE_TYPE_6_CHAM_FULL = self.classeType(id="class_type_6_cham_full", cycles=[self.CYCLE_1, self.CYCLE_2], cham=True, has_cham=True, length=2, size=1)
        self.CLASSE_TYPE_6_CHAM_NOCHAM = self.classeType(id="class_type_6_cham_nocham", cycles=[self.CYCLE_1, self.CYCLE_2], cham=False, has_cham=True, length=2, size=0.5)
        self.CLASSE_TYPE_6_NOCHAM = self.classeType(id="class_type_6_nocham", cycles=[self.CYCLE_1, self.CYCLE_2], cham=False, has_cham=False, length=2, size=1)
        self.CLASSE_TYPE_43_CHAM_FULL = self.classeType(id="class_type_43_cham_full", cycles=[self.CYCLE_1, self.CYCLE_2], cham=True, has_cham=True, length=2, size=1)
        self.CLASSE_TYPE_43_CHAM_NOCHAM = self.classeType(id="class_type_43_cham_nocham", cycles=[self.CYCLE_1, self.CYCLE_2], cham=False, has_cham=True, length=2, size=0.5)
        self.CLASSE_TYPE_43_NOCHAM = self.classeType(id="class_type_43_nocham", cycles=[self.CYCLE_1, self.CYCLE_2], cham=False, has_cham=False, length=3, size=1)

        self.classes = []
        self.CLASSE_1 = self.classe(id="classe_id_1", label="classe_label_1", level=6, index=1, type=self.CLASSE_TYPE_6_CHAM_FULL)
        self.CLASSE_6_CHAM_FULL = self.CLASSE_1
        self.CLASSE_2 = self.classe(id="classe_id_2", label="classe_label_2", level=6, index=2, type=self.CLASSE_TYPE_6_NOCHAM)
        self.CLASSE_6_NOCHAM = self.CLASSE_2
        self.CLASSE_3 = self.classe(id="classe_id_3", label="classe_label_3", level=4, index=1, type=self.CLASSE_TYPE_43_CHAM_FULL)
        self.CLASSE_4 = self.classe(id="classe_id_4", label="classe_label_4", level=4, index=2, type=self.CLASSE_TYPE_43_NOCHAM)
        self.CLASSE_4_NOCHAM = self.CLASSE_4
        self.CLASSE_6_CHAM_NOCHAM = self.classe(id="classe_id_6_1_cham_nocham", label="classe_label_6_1_cham_nocham", level=6, index=1, type=self.CLASSE_TYPE_6_CHAM_NOCHAM)
        self.CLASSE_4_CHAM_NOCHAM = self.classe(id="classe_id_4_1_cham_nocham", label="classe_label_4_1_cham_nocham", level=4, index=1, type=self.CLASSE_TYPE_43_CHAM_NOCHAM)

        self.teachers = []
        self.TEACHER_1 = self.teacher(id="teacher_id_1", label="teacher_label_1", min_hours=4, max_hours=10)
        self.TEACHER_2 = self.teacher(id="teacher_id_2", label="teacher_label_2", min_hours=4, max_hours=10)

        self.schedules = []
        self.SCHEDULE_M_1 = self.schedule(label="schedule_label_m_1", day="monday", beginning=8, end=10)
        self.SCHEDULE_M_2 = self.schedule(label="schedule_label_m_2", day="monday", beginning=10, end=12)
        self.SCHEDULE_M_3 = self.schedule(label="schedule_label_m_3", day="monday", beginning=14, end=16)
        self.SCHEDULE_M_4 = self.schedule(label="schedule_label_m_4", day="monday", beginning=16, end=18)
        self.SCHEDULE_T_1 = self.schedule(label="schedule_label_t_1", day="tuesday", beginning=8, end=10)
        self.SCHEDULE_T_2 = self.schedule(label="schedule_label_t_2", day="tuesday", beginning=10, end=12)
        self.SCHEDULE_T_3 = self.schedule(label="schedule_label_t_3", day="tuesday", beginning=14, end=16)
        self.SCHEDULE_T_4 = self.schedule(label="schedule_label_t_4", day="tuesday", beginning=16, end=18)
        self.SCHEDULE_W_1 = self.schedule(label="schedule_label_w_1", day="wednesday", beginning=8, end=11)
        self.SCHEDULE_F_1 = self.schedule(label="schedule_label_f_1", day="friday", beginning=8, end=10)
        self.SCHEDULE_F_1BIS = self.schedule(label="schedule_label_f_1bis", day="friday", beginning=9, end=11)
        self.SCHEDULE_F_2 = self.schedule(label="schedule_label_f_2", day="friday", beginning=10, end=12)

        self.sports = []
        self.SPORT_1 = self.sport(id="sport_id_1", label="sport_label_1", name="sport_label_1", cp=1, shared=False)
        self.SPORT_2 = self.sport(id="sport_id_2", label="sport_label_2", name="sport_label_2", cp=2, shared=False)
        self.SPORT_3 = self.sport(id="sport_id_3", label="sport_label_3", name="sport_label_3", cp=3, shared=False)
        self.SPORT_4 = self.sport(id="sport_id_4", label="sport_label_4", name="sport_label_4", cp=4, shared=False)
        self.SPORT_ACROSPORT = self.sport(id="acrosport", label="acrosport", name="acrosport", cp=4, shared=False)
        self.SPORT_CO = self.sport(id="course d'orientation", label="course d'orientation", name="course d'orientation", cp=1, shared=False)
        self.SPORT_GYM = self.sport(id="gymnastique", label="gymnastique", name="gymnastique", cp=1, shared=False)
        self.SPORT_NATATION = self.sport(id="natation", label="natation", name="natation", cp=1, shared=False)
        self.SPORT_VOLLEY = self.sport(id="volley", label="volley", name="volley", cp=1, shared=False)

        self.installations = []
        self.INSTALLATION_1 = self.installation(id="installation_id_1", label="installation_label_1", sports=[self.SPORT_1, self.SPORT_2], bus_required=False, length=2, capacity=1)
        self.INSTALLATION_2 = self.installation(id="installation_id_2", label="installation_label_2", sports=[self.SPORT_3, self.SPORT_4], bus_required=False, length=2, capacity=1)
        self.INSTALLATION_3 = self.installation(id="installation_id_3", label="installation_label_3", sports=[self.SPORT_3, self.SPORT_4], bus_required=False, length=2, capacity=2)
        self.INSTALLATION_3HOURS = self.installation(id="installation_id_3", label="installation_label_3", sports=[self.SPORT_3, self.SPORT_4], bus_required=False, length=3, capacity=2)

        self.forward_indexes()

TF = TestConfiguration()
TC = TF