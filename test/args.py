import logging

from test.configuration import TestConfiguration

class TestArgs(object):
    
    def __init__(self):
        self.config = TestConfiguration()
        self.log = logging.getLogger('edt')