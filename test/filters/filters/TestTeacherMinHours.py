from activity import Activity
from filters.filters.TeacherMinHours import TeacherMinHours
from test.configuration import TF
from test.args import TestArgs

def test_success():
    assert TeacherMinHours(TestArgs()).filter([
        Activity(teacher=TF.TEACHER_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_M_1),
        Activity(teacher=TF.TEACHER_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_M_2),
        Activity(teacher=TF.TEACHER_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_M_3),
        Activity(teacher=TF.TEACHER_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_M_4),
        Activity(teacher=TF.TEACHER_2, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_F_1BIS),
        Activity(teacher=TF.TEACHER_2, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_M_2),
    ])

def test_fail():
    assert not TeacherMinHours(TestArgs()).filter([
        Activity(teacher=TF.TEACHER_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_M_1),
        Activity(teacher=TF.TEACHER_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_M_2),
        Activity(teacher=TF.TEACHER_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_M_3),
        Activity(teacher=TF.TEACHER_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_M_4),
        Activity(teacher=TF.TEACHER_2, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_F_1BIS),
    ])

