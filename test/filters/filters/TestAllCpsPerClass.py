from activity import Activity
from filters.filters.AllCpsPerClass import AllCpsPerClass
from test.args import TestArgs
from test.configuration import TF

def test_success():
    assert AllCpsPerClass(TestArgs()).filter([
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_1, classe=TF.CLASSE_1, sport=TF.SPORT_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_2, classe=TF.CLASSE_1, sport=TF.SPORT_2),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_3, classe=TF.CLASSE_1, sport=TF.SPORT_3),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_4, classe=TF.CLASSE_1, sport=TF.SPORT_4),
    ])

def test_fail():
    assert not AllCpsPerClass(TestArgs()).filter([
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_1, classe=TF.CLASSE_1, sport=TF.SPORT_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_2, classe=TF.CLASSE_1, sport=TF.SPORT_2),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_3, classe=TF.CLASSE_1, sport=TF.SPORT_3),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_4, classe=TF.CLASSE_1, sport=TF.SPORT_1),
    ])