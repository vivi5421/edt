from activity import Activity
from filters.filters.HasAllClasses import HasAllClasses
from test.args import TestArgs
from test.configuration import TF

def test_success():
    assert HasAllClasses(TestArgs()).filter([
        Activity(classe=TF.CLASSE_1),
        Activity(classe=TF.CLASSE_2),
        Activity(classe=TF.CLASSE_3),
        Activity(classe=TF.CLASSE_4),
        Activity(classe=TF.CLASSE_6_CHAM_NOCHAM),
        Activity(classe=TF.CLASSE_4_CHAM_NOCHAM)
    ])

def test_fail():
    assert not HasAllClasses(TestArgs()).filter([
        Activity(classe=TF.CLASSE_1),
        Activity(classe=TF.CLASSE_1),
    ])