from activity import Activity
from filters.blockings.NeverSameSport import NeverSameSport
from test.args import TestArgs
from test.configuration import TF

def test_2sports_success():
    assert NeverSameSport(TestArgs()).filter([
        Activity(classe=TF.CLASSE_1, sport=TF.SPORT_1),
        Activity(classe=TF.CLASSE_1, sport=TF.SPORT_2),
    ])

def test_1sport_2classes_success():
    assert NeverSameSport(TestArgs()).filter([
        Activity(classe=TF.CLASSE_1, sport=TF.SPORT_1),
        Activity(classe=TF.CLASSE_2, sport=TF.SPORT_1),
    ])

def test_1sport_fail():
    assert not NeverSameSport(TestArgs()).filter([
        Activity(classe=TF.CLASSE_1, cycle=TF.CYCLE_1, sport=TF.SPORT_3),
        Activity(classe=TF.CLASSE_1, cycle=TF.CYCLE_2, sport=TF.SPORT_3),
    ])

