from activity import Activity
from filters.blockings.Min24HoursBetweenSessions import Min24HoursBetweenSessions
from test.args import TestArgs
from test.configuration import TF

def test_28hours_success():
    assert Min24HoursBetweenSessions(TestArgs()).filter([
        Activity(classe=TF.CLASSE_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_1),
        Activity(classe=TF.CLASSE_1, schedule=TF.SCHEDULE_T_3, cycle=TF.CYCLE_1),
    ])

def test_22hours_fail():
    assert not Min24HoursBetweenSessions(TestArgs()).filter([
        Activity(classe=TF.CLASSE_1, schedule=TF.SCHEDULE_M_2, cycle=TF.CYCLE_1),
        Activity(classe=TF.CLASSE_1, schedule=TF.SCHEDULE_T_1, cycle=TF.CYCLE_1),
    ])

def test_22hours__2cycles_success():
    assert Min24HoursBetweenSessions(TestArgs()).filter([
        Activity(classe=TF.CLASSE_1, schedule=TF.SCHEDULE_M_2, cycle=TF.CYCLE_1),
        Activity(classe=TF.CLASSE_1, schedule=TF.SCHEDULE_T_1, cycle=TF.CYCLE_2),
    ])

def test_same_failure():
    assert not Min24HoursBetweenSessions(TestArgs()).filter([
        Activity(classe=TF.CLASSE_1, schedule=TF.SCHEDULE_M_2, cycle=TF.CYCLE_1),
        Activity(classe=TF.CLASSE_1, schedule=TF.SCHEDULE_M_2, cycle=TF.CYCLE_1),
    ])

def test_1day_fail():
    assert not Min24HoursBetweenSessions(TestArgs()).filter([
        Activity(classe=TF.CLASSE_1, schedule=TF.SCHEDULE_M_2, cycle=TF.CYCLE_1),
        Activity(classe=TF.CLASSE_1, schedule=TF.SCHEDULE_T_2, cycle=TF.CYCLE_1),
    ])

def test_24hours_success():
    assert Min24HoursBetweenSessions(TestArgs()).filter([
        Activity(classe=TF.CLASSE_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_1),
        Activity(classe=TF.CLASSE_1, schedule=TF.SCHEDULE_T_2, cycle=TF.CYCLE_1),
    ])

def test_22hours_2classes_success():
    assert Min24HoursBetweenSessions(TestArgs()).filter([
        Activity(classe=TF.CLASSE_1, schedule=TF.SCHEDULE_M_2, cycle=TF.CYCLE_1),
        Activity(classe=TF.CLASSE_2, schedule=TF.SCHEDULE_T_1, cycle=TF.CYCLE_1),
    ])

def test_3schedules_success():
    assert Min24HoursBetweenSessions(TestArgs()).filter([
        Activity(classe=TF.CLASSE_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_1),
        Activity(classe=TF.CLASSE_1, schedule=TF.SCHEDULE_T_2, cycle=TF.CYCLE_1),
        Activity(classe=TF.CLASSE_1, schedule=TF.SCHEDULE_F_1, cycle=TF.CYCLE_1)
    ])

def test_3schedules_fail():
    assert not Min24HoursBetweenSessions(TestArgs()).filter([
        Activity(classe=TF.CLASSE_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_1),
        Activity(classe=TF.CLASSE_1, schedule=TF.SCHEDULE_T_1, cycle=TF.CYCLE_1),
        Activity(classe=TF.CLASSE_1, schedule=TF.SCHEDULE_F_1, cycle=TF.CYCLE_1)
    ])