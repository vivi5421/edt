from activity import Activity
from filters.blockings.TeacherMaxHours import TeacherMaxHours
from test.args import TestArgs
from test.configuration import TF

def test_6hours_success():
    assert TeacherMaxHours(TestArgs()).filter([
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_2, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_3, cycle=TF.CYCLE_1),
    ])

def test_10hours_success():
    assert TeacherMaxHours(TestArgs()).filter([
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_2, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_3, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_T_2, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_T_3, cycle=TF.CYCLE_1),
    ])

def test_12hours_fail():
    assert not TeacherMaxHours(TestArgs()).filter([
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_2, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_3, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_T_1, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_T_2, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_T_3, cycle=TF.CYCLE_1),
    ])

def test_2cycles_fail():
    assert not TeacherMaxHours(TestArgs()).filter([
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_2, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_3, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_T_1, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_T_2, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_T_3, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_2),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_2, cycle=TF.CYCLE_2),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_3, cycle=TF.CYCLE_2),
    ])