from activity import Activity
from filters.blockings.OneSportPerCycle import OneSportPerCycle
from test.args import TestArgs
from test.configuration import TF

def test_success():
    assert OneSportPerCycle(TestArgs()).filter([
        Activity(cycle=TF.CYCLE_1, classe=TF.CLASSE_1),
        Activity(cycle=TF.CYCLE_2, classe=TF.CLASSE_1),
        Activity(cycle=TF.CYCLE_1, classe=TF.CLASSE_2),
        Activity(cycle=TF.CYCLE_2, classe=TF.CLASSE_2),
    ])

def test_fail():
    assert not OneSportPerCycle(TestArgs()).filter([
        Activity(cycle=TF.CYCLE_1, classe=TF.CLASSE_1),
        Activity(cycle=TF.CYCLE_2, classe=TF.CLASSE_1),
        Activity(cycle=TF.CYCLE_1, classe=TF.CLASSE_1),
        Activity(cycle=TF.CYCLE_2, classe=TF.CLASSE_2),
    ])