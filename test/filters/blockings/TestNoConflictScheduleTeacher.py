from activity import Activity
from filters.blockings.NoConflictScheduleTeacher import NoConflictScheduleTeacher
from test.args import TestArgs
from test.configuration import TF

def test_noconflict_success():
    assert NoConflictScheduleTeacher(TestArgs()).filter([
        Activity(teacher=TF.TEACHER_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_M_1, classe=TF.CLASSE_1),
        Activity(teacher=TF.TEACHER_1, cycle=TF.CYCLE_2, schedule=TF.SCHEDULE_M_1, classe=TF.CLASSE_1),
        Activity(teacher=TF.TEACHER_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_M_2, classe=TF.CLASSE_2),
        Activity(teacher=TF.TEACHER_2, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_M_1, classe=TF.CLASSE_2),
    ])

def test_conflict_fail():
    assert not NoConflictScheduleTeacher(TestArgs()).filter([
        Activity(teacher=TF.TEACHER_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_M_1, classe=TF.CLASSE_1),
        Activity(teacher=TF.TEACHER_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_M_1, classe=TF.CLASSE_2),
    ])

def test_conflict_partialhours_fail():
    assert not NoConflictScheduleTeacher(TestArgs()).filter([
        Activity(teacher=TF.TEACHER_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_F_1, classe=TF.CLASSE_1),
        Activity(teacher=TF.TEACHER_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_F_1BIS, classe=TF.CLASSE_2),
    ])