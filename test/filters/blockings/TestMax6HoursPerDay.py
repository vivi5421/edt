from activity import Activity
from filters.blockings.Max6HoursPerDay import Max6HoursPerDay
from test.args import TestArgs
from test.configuration import TF

def test_4hours_success():
    assert Max6HoursPerDay(TestArgs()).filter([
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_2, cycle=TF.CYCLE_1)
    ])

def test_8hours_fail():
    assert not Max6HoursPerDay(TestArgs()).filter([
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_2, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_3, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_4, cycle=TF.CYCLE_1)
    ])

def test_8hours_2cycles_success():
    assert Max6HoursPerDay(TestArgs()).filter([
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_2, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_3, cycle=TF.CYCLE_2),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_4, cycle=TF.CYCLE_2)
    ])

def test_4hours_2days_success():
    assert Max6HoursPerDay(TestArgs()).filter([
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_T_1, cycle=TF.CYCLE_1),
    ])

def test_8hours_2days_success():
    assert Max6HoursPerDay(TestArgs()).filter([
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_1, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_M_2, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_T_1, cycle=TF.CYCLE_1),
        Activity(teacher=TF.TEACHER_1, schedule=TF.SCHEDULE_T_2, cycle=TF.CYCLE_1)
    ])