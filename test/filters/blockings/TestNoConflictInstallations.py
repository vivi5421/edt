from activity import Activity
from filters.blockings.NoConflictInstallations import NoConflictInstallations
from test.args import TestArgs
from test.configuration import TF

def test_noconflict_success():
    assert NoConflictInstallations(TestArgs()).filter([
        Activity(installation=TF.INSTALLATION_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_M_1, classe=TF.CLASSE_1),
        Activity(installation=TF.INSTALLATION_2, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_M_1, classe=TF.CLASSE_1),
        Activity(installation=TF.INSTALLATION_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_M_2, classe=TF.CLASSE_1),
        Activity(installation=TF.INSTALLATION_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_T_1, classe=TF.CLASSE_1),
    ])

def test_conflict_fail():
    assert not NoConflictInstallations(TestArgs()).filter([
        Activity(installation=TF.INSTALLATION_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_M_1, classe=TF.CLASSE_1),
        Activity(installation=TF.INSTALLATION_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_M_1, classe=TF.CLASSE_1),
    ])

def test_conflict_partialhours_fail():
    assert not NoConflictInstallations(TestArgs()).filter([
        Activity(installation=TF.INSTALLATION_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_F_1, classe=TF.CLASSE_1),
        Activity(installation=TF.INSTALLATION_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_F_1BIS, classe=TF.CLASSE_1),
    ])

def test_conflict_longcycle_fail():
    assert not NoConflictInstallations(TestArgs()).filter([
        Activity(installation=TF.INSTALLATION_1, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_F_1, classe=TF.CLASSE_1),
        Activity(installation=TF.INSTALLATION_1, cycle=TF.CYCLE_1_2, schedule=TF.SCHEDULE_F_1, classe=TF.CLASSE_2),
    ])

def test_conflict_biginstallation_success():
    assert NoConflictInstallations(TestArgs()).filter([
        Activity(installation=TF.INSTALLATION_3, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_F_1, classe=TF.CLASSE_1),
        Activity(installation=TF.INSTALLATION_3, cycle=TF.CYCLE_1, schedule=TF.SCHEDULE_F_1, classe=TF.CLASSE_2),
    ])