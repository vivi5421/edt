from activity import Activity
from filters.blockings.ClassHasSameTeacher import ClassHasSameTeacher
from test.args import TestArgs
from test.configuration import TF

def test_filter_success():
    assert ClassHasSameTeacher(TestArgs()).filter([
        Activity(classe=TF.CLASSE_1, teacher=TF.TEACHER_1),
        Activity(classe=TF.CLASSE_1, teacher=TF.TEACHER_1),
    ])

def test_filter_fail():
    assert not ClassHasSameTeacher(TestArgs()).filter([
        Activity(classe=TF.CLASSE_1, teacher=TF.TEACHER_1, cycle=TF.CYCLE_1),
        Activity(classe=TF.CLASSE_1, teacher=TF.TEACHER_2, cycle=TF.CYCLE_2),
    ])

def test_filter_1teacher_2classes_success():
    assert ClassHasSameTeacher(TestArgs()).filter([
        Activity(classe=TF.CLASSE_1, teacher=TF.TEACHER_1),
        Activity(classe=TF.CLASSE_2, teacher=TF.TEACHER_1),
    ])

def test_filter_2ndclasse_fail():
    assert not ClassHasSameTeacher(TestArgs()).filter([
        Activity(classe=TF.CLASSE_1, teacher=TF.TEACHER_1),
        Activity(classe=TF.CLASSE_1, teacher=TF.TEACHER_1),
        Activity(classe=TF.CLASSE_2, teacher=TF.TEACHER_1, cycle=TF.CYCLE_1),
        Activity(classe=TF.CLASSE_2, teacher=TF.TEACHER_2, cycle=TF.CYCLE_2),
    ])