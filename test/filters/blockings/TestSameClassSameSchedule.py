from activity import Activity
from filters.blockings.SameClassSameSchedule import SameClassSameSchedule
from test.args import TestArgs
from test.configuration import TF

def test_success():
    assert SameClassSameSchedule(TestArgs()).filter([
        Activity(cycle=TF.CYCLE_1, classe=TF.CLASSE_1, schedule=TF.SCHEDULE_F_1),
        Activity(cycle=TF.CYCLE_2, classe=TF.CLASSE_1, schedule=TF.SCHEDULE_F_1),
        Activity(cycle=TF.CYCLE_1, classe=TF.CLASSE_2, schedule=TF.SCHEDULE_M_1),
        Activity(cycle=TF.CYCLE_2, classe=TF.CLASSE_2, schedule=TF.SCHEDULE_M_1),
    ])

def test_fail():
    assert not SameClassSameSchedule(TestArgs()).filter([
        Activity(cycle=TF.CYCLE_1, classe=TF.CLASSE_1, schedule=TF.SCHEDULE_F_1),
        Activity(cycle=TF.CYCLE_2, classe=TF.CLASSE_1, schedule=TF.SCHEDULE_F_2),
    ])

def test_partial_conflicts_fail():
    assert not SameClassSameSchedule(TestArgs()).filter([
        Activity(cycle=TF.CYCLE_1, classe=TF.CLASSE_1, schedule=TF.SCHEDULE_F_1),
        Activity(cycle=TF.CYCLE_2, classe=TF.CLASSE_1, schedule=TF.SCHEDULE_F_1BIS),
    ])