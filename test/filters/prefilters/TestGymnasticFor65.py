from activity import Activity
from filters.prefilters.GymnasticFor65 import GymnasticFor65
from test.args import TestArgs
from test.configuration import TF

def test_success():
    assert GymnasticFor65(TestArgs()).filter(Activity(classe=TF.CLASSE_1, sport=TF.SPORT_GYM))

def test_fail():
    assert not GymnasticFor65(TestArgs()).filter(Activity(classe=TF.CLASSE_3, sport=TF.SPORT_GYM))