from activity import Activity
from filters.prefilters.Program2019 import Prog_2019, Conservatoire, NoPiscineCycle4, PiscineAfternoon
from test.args import TestArgs
from test.configuration import TF

def test_natation_6():
    assert not Prog_2019(TestArgs()).filter(
        Activity(classe=TF.CLASSE_4_CHAM_NOCHAM, sport=TF.SPORT_NATATION)
    )
    assert Prog_2019(TestArgs()).filter(
        Activity(classe=TF.CLASSE_6_CHAM_FULL, sport=TF.SPORT_NATATION)
    )
