from activity import Activity
from filters.prefilters.ThreeHoursSessions import ThreeHoursSessions
from test.args import TestArgs
from test.configuration import TC

def test_sportsnotforcham():
    assert not ThreeHoursSessions(TestArgs()).filter(Activity(installation=TC.INSTALLATION_3HOURS, classe=TC.CLASSE_6_CHAM_FULL))
    assert ThreeHoursSessions(TestArgs()).filter(Activity(installation=TC.INSTALLATION_3HOURS, classe=TC.CLASSE_4_NOCHAM))