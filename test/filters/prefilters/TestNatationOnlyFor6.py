from activity import Activity
from filters.prefilters.NatationOnlyFor6 import NatationOnlyFor6
from test.args import TestArgs
from test.configuration import TF

def test_success():
    assert NatationOnlyFor6(TestArgs()).filter(Activity(classe=TF.CLASSE_1, sport=TF.SPORT_NATATION))

def test_fail():
    assert not NatationOnlyFor6(TestArgs()).filter(Activity(classe=TF.CLASSE_3, sport=TF.SPORT_NATATION))