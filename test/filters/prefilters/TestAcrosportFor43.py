from activity import Activity
from filters.prefilters.AcrosportFor43 import AcrosportFor43
from test.args import TestArgs
from test.configuration import TF

def test_acrosport_6_fail():
    assert not AcrosportFor43(TestArgs()).filter(Activity(sport=TF.SPORT_ACROSPORT, classe=TF.CLASSE_1))

def test_acrosport_4_nocham_success():
    assert AcrosportFor43(TestArgs()).filter(Activity(sport=TF.SPORT_ACROSPORT, classe=TF.CLASSE_4))

def test_acrosport_4_cham_success():
    assert AcrosportFor43(TestArgs()).filter(Activity(sport=TF.SPORT_ACROSPORT, classe=TF.CLASSE_3))