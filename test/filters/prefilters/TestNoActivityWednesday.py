from activity import Activity
from filters import Filter
from test.configuration import TC

def is_valid(activity, teacher):
    '''
    >>> is_valid(Activity(teacher=TC.TEACHER_1, schedule=TC.SCHEDULE_W_1), "teacher_id_2")
    True
    >>> is_valid(Activity(teacher=TC.TEACHER_2, schedule=TC.SCHEDULE_W_1), "teacher_id_2")
    False
    '''
    return not activity.teacher.id == teacher or not activity.schedule.day == "wednesday"
