from activity import Activity
from filters.prefilters.SportsInInstallation import SportsInInstallation
from test.args import TestArgs
from test.configuration import TC

def test_sportsininstallation():
    assert SportsInInstallation(TestArgs()).filter(Activity(sport=TC.SPORT_1, installation=TC.INSTALLATION_1))
    assert SportsInInstallation(TestArgs()).filter(Activity(sport=TC.SPORT_2, installation=TC.INSTALLATION_1))
    assert not SportsInInstallation(TestArgs()).filter(Activity(sport=TC.SPORT_ACROSPORT, installation=TC.INSTALLATION_1))
