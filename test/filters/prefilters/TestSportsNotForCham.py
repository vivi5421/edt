from activity import Activity
from filters.prefilters.SportsNotForCham import SportsNotForCham
from test.args import TestArgs
from test.configuration import TC

def test_sportsnotforcham():
    assert SportsNotForCham(TestArgs()).filter(Activity(sport=TC.SPORT_1, classe=TC.CLASSE_6_CHAM_FULL))
    assert not SportsNotForCham(TestArgs()).filter(Activity(sport=TC.SPORT_VOLLEY, classe=TC.CLASSE_6_CHAM_FULL))

    assert SportsNotForCham(TestArgs()).filter(Activity(sport=TC.SPORT_1, classe=TC.CLASSE_6_NOCHAM))
    assert SportsNotForCham(TestArgs()).filter(Activity(sport=TC.SPORT_VOLLEY, classe=TC.CLASSE_6_NOCHAM))
