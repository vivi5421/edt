from activity import Activity
from filters.prefilters.COFor43NotCham import COFor43NotCham
from test.args import TestArgs
from test.configuration import TF

def test_6_cham_full():
    # assert not COFor43NotCham(TestArgs()).filter(Activity(classe=TF.CLASSE_1, sport=TF.SPORT_CO, schedule=TF.SCHEDULE_W_1))
    assert not COFor43NotCham(TestArgs()).filter(Activity(classe=TF.CLASSE_1, sport=TF.SPORT_CO, schedule=TF.SCHEDULE_M_1))

def test_6_cham_nocham():
    # assert not COFor43NotCham(TestArgs()).filter(Activity(classe=TF.CLASSE_6_CHAM_NOCHAM, sport=TF.SPORT_CO, schedule=TF.SCHEDULE_W_1))
    assert not COFor43NotCham(TestArgs()).filter(Activity(classe=TF.CLASSE_6_CHAM_NOCHAM, sport=TF.SPORT_CO, schedule=TF.SCHEDULE_M_1))

def test_6_nocham():
    assert not COFor43NotCham(TestArgs()).filter(Activity(classe=TF.CLASSE_2, sport=TF.SPORT_CO, schedule=TF.SCHEDULE_W_1))
    assert not COFor43NotCham(TestArgs()).filter(Activity(classe=TF.CLASSE_2, sport=TF.SPORT_CO, schedule=TF.SCHEDULE_M_1))

def test_4_cham_full():
    # This test does not make sense since only not cham classes can have 3 hours schedules
    # assert COFor43NotCham(TestArgs()).filter(Activity(classe=TF.CLASSE_3, sport=TF.SPORT_CO, schedule=TF.SCHEDULE_W_1))
    assert not COFor43NotCham(TestArgs()).filter(Activity(classe=TF.CLASSE_3, sport=TF.SPORT_CO, schedule=TF.SCHEDULE_M_1))

def test_4_cham_nocham():
    # assert not COFor43NotCham(TestArgs()).filter(Activity(classe=TF.CLASSE_4_CHAM_NOCHAM, sport=TF.SPORT_CO, schedule=TF.SCHEDULE_W_1))
    assert not COFor43NotCham(TestArgs()).filter(Activity(classe=TF.CLASSE_4_CHAM_NOCHAM, sport=TF.SPORT_CO, schedule=TF.SCHEDULE_M_1))

def test_4_nocham():
    assert COFor43NotCham(TestArgs()).filter(Activity(classe=TF.CLASSE_4, sport=TF.SPORT_CO, schedule=TF.SCHEDULE_W_1))
    assert not COFor43NotCham(TestArgs()).filter(Activity(classe=TF.CLASSE_4, sport=TF.SPORT_CO, schedule=TF.SCHEDULE_M_1))
