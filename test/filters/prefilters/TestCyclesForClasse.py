from activity import Activity
from filters.prefilters.CyclesForClasse import CyclesForClasse
from test.args import TestArgs
from test.configuration import TF

def test_success():
    assert CyclesForClasse(TestArgs()).filter(Activity(classe=TF.CLASSE_1, cycle=TF.CYCLE_1))
    assert CyclesForClasse(TestArgs()).filter(Activity(classe=TF.CLASSE_1, cycle=TF.CYCLE_2))

def test_fail():
    assert not CyclesForClasse(TestArgs()).filter(Activity(classe=TF.CLASSE_1, cycle=TF.CYCLE_3))
    assert not CyclesForClasse(TestArgs()).filter(Activity(classe=TF.CLASSE_1, cycle=TF.CYCLE_1_2))
