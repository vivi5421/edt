from datetime import datetime
from string import Formatter
from threading import Thread
from time import sleep

LOOP = ["-", "\\", "|", "/", "-", "/", "|", "/"]

class SciFormatter(Formatter):

    def convert_field(self, value, conversion):
        precision = 3
        max_length = 7
        sep = 'e'

        if conversion == 'k':
            str_num = str(value)
            if len(str_num) > max_length:
                return "{}.{}{}{}".format(str_num[0], str_num[1:precision + 1], sep, len(str_num) - 1)
            return "{:n}".format(value)
        else:
            return super().convert_field(value, conversion)

def counter(progress):
    while not progress._is_finished:
        progress._loop = (progress._loop + 1) % len(LOOP)
        progress.print()
        sleep(progress._delay)    

class Progress(object):

    def __init__(self, label=None, max=None, delay=0, bar_length=80):
        if label is None:
            if max:
                self._label = "Processing... {bar:100} ({current}/{max})"
            else:
                self._label = "Processing... ({current}) {loop}"
        else:
            self._label = label
        self._max = max
        self._delay = delay
        self._loop = 0
        self._bar_length = bar_length
        # This is used in case the new line is shorter than the new one, to remove chars at the end
        self._last_line_length = 1
        self._step = 0
        self._is_finished = False
        self._fmt = SciFormatter()
    
    def __enter__(self):
        self._current = 0
        self._current_time = datetime.now()
        self._percent = 0
        self._elapsed = 0
        self._step = 0
        self._remaining = self._max
        self._start = datetime.now()
        self._loop = 0
        if self._delay > 0:
            t = Thread(target=counter, args=(self,), daemon=True)
            t.start()
        if self._delay >= 0:
            self.print()
        return self

    def next(self, step=1):
        self._current += step
        self._step += step
        if self._delay == 0:
            self.print()

    def print(self):
        _now = datetime.now()
        step = self._step
        current = self._current
        gelapsed = _now - self._start
        gaverage = current // int(gelapsed.total_seconds()) if int(gelapsed.total_seconds()) > 0 else 0
        elapsed = _now - self._current_time
        # We take advantage n_current)time is not updated yet to get the last one
        average = step // int(elapsed.total_seconds()) if int(elapsed.total_seconds()) > 0 else 0
        self._current_time = datetime.now()
        if self._max:
            max = self._max
            percent = self._current / self._max * 100
            remaining = self._max - self._current
        if 'loop' in self._label:
            loop = LOOP[self._loop] if not self._is_finished else " "
        if 'bar' in self._label:
            try:
                limit = int(self._current * self._bar_length // self._max)
            except OverflowError:
                limit = 0
            bar = "|{done}{todo}|".format(
                done='#' * limit,
                todo='-' * (self._bar_length - limit)
            )
        line = self._fmt.format(self._label, **locals())
        print("\r{{:{padding}}}".format(padding=self._last_line_length).format(line), end='')
        self._last_line_length = len(line)
        self._step = 0    
    
    def __exit__(self, *args):
        self._is_finished = True
        if self._max:
            self._current = self._max
        self._percent = 100
        if self._delay >= 0:
            self.print()
            print("\n")