class Progress(object):

    def __init__(self, title, max, opti_cpu=False, multiline=False):
        self.title = title
        self.max = max
        self.opti_cpu = opti_cpu
        self.multiline = multiline
    
    # @property
    # def percent(self):
    #     return self.current / self.max * 100

    def __enter__(self):
        self.current = 0
        self.percent = 0
        self.print()
        return self
    
    def __exit__(self, *args):
        self.finish()
    
    def print(self):
        print("{carriage}|{done}{remaining}| {title} {percent}% ({max})".format(
            carriage="\n" if self.multiline else "\r",
            done="#"*int(self.percent),
            remaining="-"*(100 - int(self.percent)),
            title=self.title,
            percent=int(self.percent),
            max=self.max),
            end=""
        ) 

    def next(self, step=1):
        self.current += step
        self.percent = self.current / self.max * 100
        if self.opti_cpu:
            if self.percent % 1 == 0:
                self.print()
        else:
            self.print()

    def finish(self):
        print("Finished")

        g5#2xTm9UcDcwb4oqbMbLHQ!a6noKI#gV&SW
        