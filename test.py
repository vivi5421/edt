import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

class OObject(object):

    def __init__(self):
        self.id = "<not defined>"
    
    def __str__(self):
        return "<{}: {}>".format(self.__class__.__name__, self.__dict__)
    
    def __repr__(self):
        return "<{}: {}>".format(self.__class__.__name__, self.id)
    
    def __eq__(self, other): 
        return self.__dict__ == other.__dict__
    
    def __hash__(self):
        return hash(self.id)
    
    @classmethod
    def get(cls, key="<all>"):
        objs = [obj for obj in gc.get_objects() if isinstance(obj, cls)]
        if key == "<all>":
            return objs
        else:
            try:
                return [obj for obj in objs if obj.id == key][0]
            except IndexError as e:
                logger.error("Cannot get item [{}] from list [{}]".format(key, objs))
                logger.error(e)

class Teacher(OObject):
    def __init__(self, firstname=None, lastname=None, min_hours=None, max_hours=None):
        self.id = lastname
        self.firstname = firstname
        self.lastname = lastname
        self.min_hours = min_hours
        self.max_hours = max_hours

class Schedule(OObject):
    def __init__(self, day=None, beginning=None, end=None):
        self.id = "{} {}-{}".format(day, beginning, end)
        self.day = day
        self.beginning = beginning
        self.end = end
    
    def elapsed_time(self, end=False):
        days = 1 if self.day == "Tuesday" else 2 if self.day == "Wednesday" else 3 if self.day == "Thursday" else 4 if self.day == "Friday" else 0
        return days * 24 + (self.end if end else self.beginning)
    
    def length(self):
        return self.end - self.beginning

class Activity(OObject):
    def __init__(self, cycle=None, classe=None, teacher=None, installation=None, sport=None, schedule=None):
        self.id = "{}/{}/{}/{}/{}/{}".format(repr(cycle), repr(classe), repr(teacher), repr(installation), repr(sport), repr(schedule))
        self.cycle = cycle
        self.classe = classe
        self.teacher = teacher
        self.installation = installation
        self.sport = sport
        self.schedule = schedule

a1 = Activity(teacher=Teacher("prof1"), schedule=Schedule("Monday", 8, 12))
a2 = Activity(teacher=Teacher("prof1"), schedule=Schedule("Monday", 14, 18))
llist = [a1, a2]

def get_teachers():
    tmp = [act.teacher for act in llist]
    return list(set(tmp))

hours_day_teacher = {}
for teacher in get_teachers():
    hours_day_teacher[teacher] = {}
    for act in llist:
        if act.schedule.day in hours_day_teacher[teacher]:
            print("add")
            hours_day_teacher[teacher][act.schedule.day] += act.schedule.length()
        else:
            print("new")
            hours_day_teacher[teacher][act.schedule.day] = act.schedule.length()
        print(hours_day_teacher)
        if any([x > 6 for x in hours_day_teacher[teacher].values()]):
            print(False)
print(True)