from model import OObject

class Installation(OObject):

    def __init__(self, id=None, label=None, _sports=None, bus_required=False, length=None, capacity=None):
        self.id = id
        self.label = label
        self._sports = _sports
        self.sports = None
        self.bus_required = bus_required
        self.length = length
        self.capacity = capacity
