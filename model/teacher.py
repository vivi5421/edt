from model import OObject

class Teacher(OObject):

    def __init__(self, id=None, label=None, min_hours=None, max_hours=None):
        self.id = id
        self.label = label
        self.min_hours = min_hours
        self.max_hours = max_hours