from model import OObject

class ClasseType(OObject):

    def __init__(self, id=None, _cycles=None, cham=False, has_cham=False, sessions=1, length=2, size=1):
        self.id = id
        self._cycles = _cycles
        self.cycles = None
        self.cham = cham
        self.has_cham = has_cham
        self.length = length
        self.size = size