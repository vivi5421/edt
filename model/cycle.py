from model import OObject

class Cycle(OObject):

    def __init__(self, id=None, periods=None, label=None):
        self.id = id
        self.periods = periods
        self.label = label

