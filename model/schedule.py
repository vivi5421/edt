from model import OObject

DAYS = {
        "monday": 0 * 24,
        "tuesday": 1 * 24,
        "wednesday": 2 * 24,
        "thursday": 3 * 24,
        "friday": 4 * 24
    }

class Schedule(OObject):

    def __init__(self, label=None, day=None, beginning=None, end=None):
        self.id = "{}-{}-{}".format(day, beginning, end)
        self.day = day
        self.beginning = beginning
        self.end = end
        self.label = label
    
    def time(self, end=False):
        return DAYS[self.day] + (self.end if end else self.beginning)
    
    def length(self):
        return self.end - self.beginning
    
    def __lt__(self, other):
        return self.time() < other.time()