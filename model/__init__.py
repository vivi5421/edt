import functools
import gc
import sys
import traceback

UNDEFINED = "<undefined>"

class OObject(object):

    def __init__(self):
        self.id = UNDEFINED
        self.label = UNDEFINED

    def __str__(self):
        #return "<{}: {}>".format(self.__class__.__name__, self.__dict__)
        return "<{}>".format(self.id)
    
    def __repr__(self):
        #return "<{}: {}>".format(self.__class__.__name__, self.id)
        return self.__str__()
    
    def __eq__(self, other):
        if other is None:
            return False 
        return self.__dict__ == other.__dict__
    
    def __hash__(self):
        return hash(self.id)
    
    def __lt__(self, other):
        return self.id < other.id
