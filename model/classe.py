from model import OObject

class Classe(OObject):

    def __init__(self, id=None, label=None, level=None, index=None, _type=None):
        self.id = id
        self.level = level
        self.index = index
        self.label = label
        self._type = _type
        self.type = None