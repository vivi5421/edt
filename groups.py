from utils import group

SORT_CLASSE = lambda x: x.classe
SORT_CLASSE_CYCLE = lambda x: (x.classe, x.cycle)
SORT_TEACHER = lambda x: x.teacher
SORT_TEACHER_CYCLE = lambda x: (x.teacher, x.cycle)
SORT_TEACHER_CYCLE_SCHEDULE_DAY = lambda x: (x.teacher, x.cycle, x.schedule.day)
SORT_TEACHER_CYCLE_SCHEDULE = lambda x: (x.teacher, x.cycle, x.schedule)
SORT_INSTALLATION = lambda x: x.installation
SORT_INSTALLATION_PERIOD = lambda x: (x.installation, [period in x.cycle.periods for period in range(1, 5)])
SORT_INSTALLATION_CYCLE_SCHEDULE = lambda x: (x.installation, x.cycle, x.schedule)


class Groups(object):

    def __init__(self, activities):
        self.activities = activities
        self.cache = {} 
        
    def get(self, sort):
        try:
            return self.cache[sort]
        except:
            result = group(self.activities, sort)
            self.cache[sort] = result
            return result
        
        
        
        
        
        
         
        