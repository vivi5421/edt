import logging
from activity import Activity
from model.classe import Classe
from model.classe_type import ClasseType
from model.cycle import Cycle
from model.installation import Installation
from model.schedule import Schedule
from model.sport import Sport
from model.teacher import Teacher

logger = logging.getLogger("edt")

def get_from(id, llist):
    try:
        return [x for x in llist if x.id == id][0]
    except IndexError as ie:
        print(ie)


def _sanity(ttype, llist, expected):
    for item in llist:
        if not isinstance(item, expected):
            print("{} {} is invalid".format(ttype, item))


class Configuration(object):

    def __init__(self):
        self.schedules = []
        self.cycles = []
        self.sports = []
        self.installations = []
        self.classeTypes = []
        self.classes = []
        self.teachers = []

    def __str__(self):
        return "\n".join([
            "SCHEDULES: {self.schedules}",
            "CYCLES: {self.cycles}",
            "SPORTS: {self.sports}",
            "INSTALLATIONS: {self.installations}",
            "CLASSE_TYPES: {self.classeTypes}",
            "CLASSES: {self.classes}",
            "TEACHERS: {self.teachers}",
        ]).format(self=self)
    
    def __repr__(self):
        return "<ConfigFile>"

    def get_schedule(self, id):
        return get_from(id, self.schedules)

    def get_cycle(self, id):
        return get_from(id, self.cycles)

    def get_sport(self, id):
        return get_from(id, self.sports)
    
    def get_installation(self, id):
        return get_from(id, self.installations)
    
    def get_classe_type(self, id):
        return get_from(id, self.classeTypes)
    
    def get_classe(self, id):
        return get_from(id, self.classes)
    
    def get_teacher(self, id):
        return get_from(id, self.teachers)
    
    def forward_indexes(self):
        for classeType in self.classeTypes:
                classeType.cycles = [self.get_cycle(iid) for iid in classeType._cycles]
        for classe in self.classes:
            classe.type = self.get_classe_type(classe._type)
        for installation in self.installations:
            installation.sports = [self.get_sport(iid) for iid in installation._sports]

    def gener_activities(self, args, preffilters):
        for cycle in sorted(self.cycles):
            for classe in sorted(self.classes):
                for teacher in sorted(self.teachers):
                    for installation in sorted(self.installations):
                        for sport in sorted(self.sports):
                            for schedule in sorted(self.schedules):
                                act = Activity(cycle=cycle, classe=classe, teacher=teacher, installation=installation, sport=sport, schedule=schedule)
                                if all(preffilter.filter(act) for preffilter in preffilters):
                                    yield act
                                else:
                                    if logger.isEnabledFor(logging.DEBUG) and args.display_prefilters:
                                        result = [ffilter for ffilter in preffilters if not ffilter.filter(act)]
                                        logger.debug("Filtered-out: {} ({})".format(act, result))

    def sanity(self):
        _sanity("schedule", self.schedules, Schedule)
        _sanity("cycle", self.cycles, Cycle)
        _sanity("sport", self.sports, Sport)
        _sanity("installation", self.installations, Installation)
        _sanity("classeType", self.classeTypes, ClasseType)
        _sanity("classe", self.classes, Classe)
        _sanity("teacher", self.teachers, Teacher)
                                