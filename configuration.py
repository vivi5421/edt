class Configuration(object):
    def __init__(self):
        self.schedules = []
        self.cycles = []
        self.sports = []
        self.installations = []
        self.classes = []
        self.teachers = []

    def __str__(self):
        return "\n".join([
            "SCHEDULES: {self.schedules}",
            "CYCLES: {self.cycles}",
            "SPORTS: {self.sports}",
            "INSTALLATIONS: {self.installations}",
            "CLASSES: {self.classes}",
            "TEACHERS: {self.teachers}"
        ]).format(self=self)
    
    def __repr__(self):
        return self.__str__()