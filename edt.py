import argparse
import contextlib
import custom_logger
import datetime
import importlib
import itertools
import gc
import glob
import logging
import math
import os
import progressbar
import shutil
import subprocess
import sys
from operator import attrgetter
from functools import reduce
from configuration import Configuration
from model import OObject
from model import Schedule
from model import Cycle
from model import Sport
from model import Installation
from model import Classe
from model import Teacher
from activity import Activity
from activity import Activities
from constraints.blocking import BlockingConstraint
from constraints.validation import ValidationConstraint

try:
    import jsonpickle
except ImportError:
    subprocess.check_call(["python", "-m", "pip", "install", "--user", "-q", "jsonpickle"])
    import jsonpickle

try:
    import prettytable
except ImportError:
    subprocess.check_call(["python", "-m", "pip", "install", "--user", "-q", "prettytable"])
    import prettytable

# try:
#     from progress.bar import Bar
#     from progress.spinner import Spinner
# except ImportError:
#     subprocess.check_call(["python", "-m", "pip", "install", "--user", "-q", "progress"])
#     from progress.bar import Bar
#     from progress.spinner import Spinner

try:
    from six.moves import input
except ImportError:
    subprocess.check_call(["python", "-m", "pip", "install", "--user", "-q", "six"])
    from six.moves import input

try:
    import colorlog
except ImportError:
    subprocess.check_call(["python", "-m", "pip", "install", "--user", "-q", "colorlog"])
    import colorlog

logger = logging.getLogger("edt")

# ValidationConstraint same sports for not cham classes for each level
# ValidationConstraint same sports for cham classes for each level

def add_to_list(llist, item):
    return llist + [item]

def remove_from_list(llist, item):
    """
    >>> remove_from_list([1, 2, 3, 4, 5], 5)
    [1, 2, 3, 4]
    >>> remove_from_list([1, 2, 3, 4, 5], 1)
    [2, 3, 4, 5]
    >>> remove_from_list([1, 2, 3, 4, 5], 3)
    [1, 2, 4, 5]
    """
    return llist[0:llist.index(item)] + llist[llist.index(item)+1:]

def int_to_sci(nb):
    str_nb = str(nb)
    if len(str_nb) < 8:
        return str_nb
    return "{}.{}{}E{}".format(str_nb[0], str_nb[1], str_nb[2], len(str_nb) - 1)

def str_list(first_line, llist):
    if llist:
        ptable = prettytable.PrettyTable()
        attrs = [key for key in llist[0].__dict__.keys() if not key == "id"]
        ptable.field_names = ["n"] + attrs
        for item in llist:
            ptable.add_row([llist.index(item)] + [repr(item.__dict__[attr]) for attr in attrs])
        return "{}:\n{}".format(first_line, ptable)
    else:
        return "{}:\n<No Element>".format(first_line)

def step(args):
    if args.step_by_step:
        input("\nPress <Enter> for next step")
        print("Resume...")

def import_constraints():
    for ttype in ["blocking", "validation"]:
        for ffile in glob.glob(os.path.join("constraints", ttype, "*.py")):
            if not "__init__" in ffile:
                importlib.import_module(ffile.replace(os.path.sep, ".").replace(".py", ""))

def get_blocking_constraints():
    return [constraint(args) for constraint in BlockingConstraint.__subclasses__()]

def get_validation_constraints():
    return [constraint(args) for constraint in ValidationConstraint.__subclasses__()]

def check_constraints(args, name, constraints, current):
    constraints_passed = True
    logger.debug("{}...".format(name))
    for constraint in constraints:
        if not constraint._iif(current):
            constraints_passed = False
    logger.debug("{} constraints Passed: {}".format(name, constraints_passed))
    return constraints_passed

def clean_impossible_activities(args, constraints, current, available):
    removes = {}
    for constraint in constraints:
        removes[constraint.__class__.__name__] = constraint._clean(current, available)
    cleaned = set(itertools.chain(*removes.values()))
    next_available = [x for x in available if x not in cleaned]
    if logger.isEnabledFor(logging.DEBUG):
        if len(cleaned):
            logger.debug("Cleaned Activities: {}/{}".format(len(cleaned), len(available)))
            if args.see_removed:
                for k, v in removes.items():
                    logger.debug(str_list(k, v))
            logger.debug("Remaining combinations: {}".format(int_to_sci(2**(len(next_available)))))
            if logger.isEnabledFor(logging.DEBUG) and args.see_remaining:
                logger.debug(str_list("Remaining", next_available))
        else:
            logger.debug("No activity removed")
    return next_available

def _process(args, valids, current, available, progress):
    #progress.next()
    with section("x"):
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(str_list("Current", current))
        blocking_constraints = get_blocking_constraints()
        blocking_constraints_passed = check_constraints(args, "Blocking", blocking_constraints, current)
        if blocking_constraints_passed:
            if args.clean:
                next_available = clean_impossible_activities(args, blocking_constraints, current, available)
            else:
                next_available = available
            validation_constraints_passed = check_constraints(args, "Validation", get_validation_constraints(), current)
            if validation_constraints_passed:
                valids.append(current)
                logger.warn(str_list("New valid schedule", current))
            else:
                step(args)
                for next_activity in next_available:
                    _process(args, valids, add_to_list(current, next_activity), remove_from_list(next_available[:], next_activity), progress)
        logger.debug("End of parsing for [current={}]".format(current))
        step(args)

def process(args, activities, combinations):
    progress = Bar("Processing... ", max=combinations, suffix="%(index)d (%(percent)d%%) [ %(elapsed)ds ]")
    valids = Activities([])
    _process(args, valids, Activities([]), activities, progress)
    progress.finish()
    return valids

def get_options():
    parser = argparse.ArgumentParser(description="Generate schedules for EPS teachers.")
    parser.add_argument("config_file", help="The config file to load", nargs="?", default="test.json")
    parser.add_argument("-s", "--step-by-step", dest="step_by_step", help="Do step-by-step mode", action="store_true")
    parser.add_argument("-d", "--debug", help="Display debug messages", action="store_true")
    parser.add_argument("-c", "--clean", help="Clean impossible activities during the process", action="store_true")
    parser.add_argument("--see-removed", dest="see_removed", help="See the cleanup activities at each step", action="store_true")
    parser.add_argument("--see-remaining", dest="see_remaining", help="See the remaining activities at each step", action="store_true")
    return parser.parse_args()

def set_logger(args):
    shutil.rmtree("logs", ignore_errors=True)
    os.mkdir("logs")
    logger.setLevel(logging.DEBUG if args.debug else logging.INFO)
    formatter = colorlog.ColoredFormatter("%(log_color)s%(asctime)s :: %(levelname)s :: %(message)s%(reset)s")
    for handler in [logging.StreamHandler(sys.stdout), custom_logger.RollingFileHandler("logs/edt.log", "w", maxBytes=1024*1024*10)]:
        handler.setLevel(logging.DEBUG)
        handler.setFormatter(formatter)
        logger.addHandler(handler)

@contextlib.contextmanager
def section(title):
    try:
        logger.info("{}  BEGIN {}  {}".format("#"*int(80-10-len(title)/2), title.upper(), "#"*int(80-10-len(title)/2)))
        yield
    finally:
        logger.info("{}  END   {}  {}".format("#"*int(80-10-len(title)/2), title.upper(), "#"*int(80-10-len(title)/2)))

def gener_activities(config):
    with progressbar.Progress("Generating activities...", max=len(config.cycles) * len(config.classes) * len(config.teachers) * len(config.installations) * len(config.sports) * len(config.schedules), opti_cpu=True) as progress:
        activities = []
        for cycle in config.cycles:
            for classe in config.classes:
                for teacher in config.teachers:
                    for installation in config.installations:
                        for sport in installation.sports:
                            for schedule in config.schedules:
                                activities.append(Activity(cycle, classe, teacher, installation, sport, schedule))
                                progress.next()
    return Activities(activities)

if __name__ == "__main__":
    args = get_options()
    set_logger(args)
    if args.debug:
        logger.setLevel(logging.DEBUG)
    with section("edt"):
        logger.info(args)
        #with open(args.config_file, "r", encoding="utf-8") as f:
        with section("configuration"):
            with open(args.config_file, "r") as f:
                config = jsonpickle.decode("".join(f.readlines()))
            logger.info("=== Configuration ===\n{}".format(config))
            import_constraints()
            logger.info("=== BlockingConstraints ===\n{}".format("\n".join([constraint.__class__.__name__ for constraint in get_blocking_constraints()])))
            logger.info("=== ValidationConstraints ===\n{}".format("\n".join([constraint.__class__.__name__ for constraint in get_validation_constraints()])))
        with section("generating activities"):
            activities = gener_activities(config)
            logger.info("Available activities: {}".format(len(activities)))
            combinations = 2**len(activities)
            logger.info("Available combinations: {}".format(int_to_sci(combinations)))
        with section("process"):
            valids = process(args, activities, combinations)
        with section("Final results"):
            logger.info("Valid schedules: {}".format(len(valids)))
            logger.info("valids:\n{}".format("\n".join([str(act) for act in valids])))