
#! /usr/bin/python3
import inspect
import locale
import logging
import os
import sys
import traceback

from args import get_options
from configuration import Configuration
from contextlib import contextmanager
from custom_logger import set_logger
from filters import Filter
from functools import partial
from glob import glob
from importlib import import_module
from progressbar import Progress
from recurse import recurse
from six.moves import input
from test_setup_exception import TestSetupException
from threading import Thread
from time import sleep
from utils import binom, sci, str_list

logger = logging.getLogger("edt")

def saver(args):
    while not args.is_process_finished:
        sleep(args.save_delay)
        args.is_save_required = True

def validate(ffilters, args, activities, indices, groups):
    reasons = []
    # Small optimization to process all filters only in case of debug
    for ffilter in ffilters:
        if not ffilter.filter(activities, groups):
            reasons.append(ffilter)
            if not logger.isEnabledFor(logging.DEBUG):
                break
    result = len(reasons) == 0
    if logger.isEnabledFor(logging.DEBUG):
        logger.debug("{} => {}\nCause: {}\n{}".format(indices, result, reasons, str_list(activities, rank=True)))
    return result

def process(args, iterable, r, validate_full, validate_blocking, mmax):
    if args.save_delay > 0:
        # pylint: disable=unexpected-keyword-arg
        t = Thread(target=saver, args=(args,), daemon=True)
        t.start()
    with Progress(label="Processing... {bar} {current!k} ({percent:.2f}%) [#{average!k}/s ##{gaverage!k}/s]", max=mmax, delay=args.progress_bar) as progress:
        return recurse(0, args, iterable, r, validate_blocking, validate_full, progress)

@contextmanager
def section(args, title):
    try:
        logger.info("{}  BEGIN {}  {}".format("#"*int(80-10-len(title)/2), title.upper(), "#"*int(80-10-len(title)/2)))
        yield
    finally:
        logger.info("{}  END   {}  {}".format("#"*int(80-10-len(title)/2), title.upper(), "#"*int(80-10-len(title)/2)))

def import_filters(args, type):
    llist = []
    for ffile in glob(os.path.join("filters", type, "*.py")):
        module = import_module(ffile.replace(os.path.sep, ".").replace(".py", ""))
        for classe in inspect.getmembers(sys.modules[module.__name__], inspect.isclass):
            if issubclass(classe[1], Filter) and not classe[0] in ["Filter"]:
                llist.append(classe[1](args))
    #return sorted(llist, key=args.sort_filters)
    return llist

def run(args):
    with section(args, "edt"):
        logger.info(args)
        with section(args, "configuration"):
            logger.info("=== Configuration ===\n{}".format(args.config))
        
        with section(args, "generate filters"):
            ffilters = import_filters(args, "filters")
            logger.info("Filters: {}".format(ffilters))

            blockings = import_filters(args, "blockings")
            logger.info("Blockings: {}".format(blockings))

            preffilters = import_filters(args, "prefilters")
            logger.info("Prefilters: {}".format(preffilters))

        with section(args, "generating activities"):
            try:
                with Progress(label="Processing... {current!k} {loop}", delay=1) as progress:
                    activities = []
                    for item in args.config.gener_activities(args, preffilters):
                        activities.append(item)
                        progress.next()
                logger.info("Available activities: {}".format(sci(len(activities))))
            except MemoryError as e:
                print(e)
                logger.error("Got a MemoryError. Fallbacking to a less verbose implementation...")
                with Progress(label="Processing... {current!k} {loop}", delay=1) as progress:
                    activities = args.config.gener_activities(args, preffilters)
        
        with section(args, "calculating combinations"):
            number_required_activities = sum([len(classe.type.cycles) for classe in args.config.classes])
            logger.info("Number of required activities: {}".format(number_required_activities))
            if not args.test_setup and len(activities) < number_required_activities:
                logger.warning("Not enough activities to get a full schedule. Stopping...")
                exit(200)
            try:
                mmax = binom(number_required_activities, len(activities))
            except ImportError:
                logger.warning(traceback.print_exc())
                mmax = 1
            logger.info("We have around {} combinations to process".format("{}".format(sci(mmax)) if mmax else "<UNKNOWN>"))

        with section(args, "process"):
            validate_full = partial(validate, ffilters=ffilters)
            validate_blocking = partial(validate, ffilters=blockings)
            args.is_process_started = True
            try:
                valids = process(args, activities, number_required_activities, validate_full, validate_blocking, mmax)
            except TestSetupException as e:
                # In this case, we have the _recurse method which is returned
                return e.method, args
            args.is_process_finished = True
            logger.info("Print valid solutions:")
            for valid in valids:
                with open(args.valids_file, "w" if args.restart else "a") as f:
                    f.write("{}\n".format(valid))
                if args.valids:
                    logger.info(valid)
            logging.info("We finished to parse solutions")

        with section(args, "Final results"):
            for i in valids:
                logger.info(i)
            logger.info("Valid schedules: {}".format(len(list(valids))))

def edt(params=None):
    args = get_options(params)
    set_logger(args)
    if args.test_setup:
        logger.warning("TEST MODE")
    locale.setlocale(locale.LC_ALL, '')
    sys.setrecursionlimit(args.recursion_limit)
    if args.profiling:
        import cProfile
        import pstats
        profiling_raw = os.path.join("logs", "profiling")
        try:
            logger.warning("Profiler mode enabled")
            cProfile.runctx("run(args)", globals(), locals(), profiling_raw)
        finally:
            logger.warning("Convert profiling results to readable format...")
            p = pstats.Stats(profiling_raw)
            p.strip_dirs().sort_stats("cumulative").print_stats()
    else:
        return run(args)

if __name__ == "__main__":
    edt()
