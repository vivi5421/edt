from constraints.blocking import BlockingConstraint

class DifferentSportPerCycle(BlockingConstraint):
    """
    >>> DifferentSportPerCycle().iif([Activity(cycle=Cycle(1), sport=Sport("sport1"), classe=Classe(6, 2)), Activity(cycle=Cycle(2), sport=Sport("sport2"), classe=Classe(6, 2))])
    True
    >>> DifferentSportPerCycle().iif([Activity(cycle=Cycle(1), sport=Sport("sport1"), classe=Classe(6, 2)), Activity(cycle=Cycle(2), sport=Sport("sport1"), classe=Classe(6, 2))])
    False
    >>> DifferentSportPerCycle().iif([Activity(cycle=Cycle(1), sport=Sport("sport1"), classe=Classe(6, 1)), Activity(cycle=Cycle(1), sport=Sport("sport2"), classe=Classe(6, 2))])
    True
    """
    def iif(self, activities):
        self.sports_per_classe = {}
        for classe in activities.get_classes():
            self.sports_per_classe[classe] = []
            for cycle in activities.get_cycles(classe=classe):
                sports_cycle = activities.get_sports(classe=classe, cycle=cycle)
                if len(sports_cycle) == 1 and not sports_cycle[0] in self.sports_per_classe[classe]:
                    self.sports_per_classe[classe].append(sports_cycle[0])
                else:
                    return False
        return True
    
    def clean(self, activities, remaining):
        remove = []
        for act in remaining:
            if act.classe in self.sports_per_classe and act.sport in self.sports_per_classe[act.classe]:
                remove.append(act)
        return remove