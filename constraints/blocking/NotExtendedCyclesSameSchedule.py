from constraints.blocking import BlockingConstraint

class NotExtendedCyclesSameSchedule(BlockingConstraint):
    def iif(self, activities):
        self.not_extended_schedule_class = {}
        for classe in activities.get_classes():
            not_extended_activities_classe = [act for act in activities.get_activities(classe=classe) if not act.cycle.extended]
            if any([not act_1.schedule == act_2.schedule for act_1 in not_extended_activities_classe for act_2 in not_extended_activities_classe]):
                return False
            else:
                self.not_extended_schedule_class[classe] = not_extended_activities_classe[0].schedule
        return True
    
    def clean(self, activities, remaining):
        remove = []
        for act in remaining:
            if not act.cycle.extended and act.classe in self.not_extended_schedule_class and not act.schedule == self.not_extended_schedule_class[act.classe]:
                remove.append(act)
        return remove