from activity import Activities
from constraints.blocking import BlockingConstraint

# Issue because a teacher has same hours all along the year, but inclduing the 4/2 cycles variations 
class NoScheduleConflictTeacher(BlockingConstraint):
    """
    >>> NoScheduleConflictTeacher().iif([Activity(teacher=Teacher("prof1", "prof1"), schedule=Schedule("Monday", 8, 10), cycle=Cycle(1)), Activity(classe=Classe(6, 2, None, None, None), teacher=Teacher("prof1", "prof1"), schedule=Schedule("Monday", 9, 11), cycle=Cycle(1))])
    False
    >>> NoScheduleConflictTeacher().iif([Activity(teacher=Teacher("prof1", "prof1"), schedule=Schedule("Monday", 8, 10), cycle=Cycle(1)), Activity(classe=Classe(6, 2, None, None, None), teacher=Teacher("prof1", "prof1"), schedule=Schedule("Monday", 10, 12), cycle=Cycle(1))])
    True
    """
    def iif(self, activities):
        self.schedules_per_teacher = {}
        for teacher in activities.get_teachers():
            self.schedules_per_teacher[teacher] = []
            teacher_activities = activities.get_activities(teacher=teacher)
            result = teacher_activities.conflicts()
            if result:
                return False
            else:
                [self.schedules_per_teacher[teacher].append(act.schedule) for act in teacher_activities]
        return True
    
    def clean(self, activities, remaining):
        remove = []
        for act in remaining:
            llist = activities.get_activities(teacher=act.teacher) + [act]
            if act.teacher in self.schedules_per_teacher and llist.conflicts():
                remove.append(act)
        return remove