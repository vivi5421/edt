from constraints.blocking import BlockingConstraint

class NoMoreThanOneSportPerCycle(BlockingConstraint):
    def iif(self, activities):
        self.cycles_classe = {}
        for classe in activities.get_classes():
            self.cycles_classe[classe] = []
            for cycle in activities.get_cycles(classe=classe):
                activities_cycle_classe = activities.get_activities(classe=classe, cycle=cycle)
                if len(activities_cycle_classe) > 1:
                    return False
                self.cycles_classe[classe].append(cycle)
        return True
    
    def clean(self, activities, remaining):
        remove = []
        for act in remaining:
            if act.classe in self.cycles_classe and act.cycle in self.cycles_classe[act.classe]:
                remove.append(act)
        return remove