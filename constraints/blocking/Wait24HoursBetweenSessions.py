from constraints.blocking import BlockingConstraint

class Wait24HoursBetweenSessions(BlockingConstraint):
    def iif(self, activities):
        for classe in activities.get_classes():
            classe_schedules = activities.get_schedules(classe=classe)
            for i in range(0, len(classe_schedules)):
                for j in range(i + 1, len(classe_schedules)):
                    if abs(classe_schedules[i].elapsed_time() - classe_schedules[j].elapsed_time(end=True)) < 24:
                        return False
                    if abs(classe_schedules[i].elapsed_time(end=True) - classe_schedules[j].elapsed_time()) < 24:
                        return False
        return True