from constraints.blocking import BlockingConstraint

class SameTeacherForSameClass(BlockingConstraint):
    def iif(self, activities):
        """
        >>> SameTeacherForSameClass().iif([Activity(classe=Classe(6, 1, None, None, None), teacher=Teacher("prof1", "prof1")), Activity(classe=Classe(6, 1, None, None, None), teacher=Teacher("prof2", "prof2"))])
        False
        >>> SameTeacherForSameClass().iif([Activity(classe=Classe(6, 1, None, None, None), teacher=Teacher("prof1", "prof1")), Activity(classe=Classe(6, 1, None, None, None), teacher=Teacher("prof1", "prof1"))])
        True
        """
        self.teacher_per_class = {}
        for classe in activities.get_classes():
            teachers = activities.get_teachers(classe=classe)
            if len(teachers) == 1:
                self.teacher_per_class[classe] = teachers[0]
            else:
                return False
        return True
    
    def clean(self, activities, remaining):
        remove = []
        for act in remaining:
            if act.classe in self.teacher_per_class.keys() and not act.teacher == self.teacher_per_class[act.classe]:
                remove.append(act)
        return remove