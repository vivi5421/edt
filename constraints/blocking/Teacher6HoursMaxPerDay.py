from constraints.blocking import BlockingConstraint

class Teacher6HoursMaxPerDay(BlockingConstraint):
    def iif(self, activities):
        """
        >>> Teacher6HoursMaxPerDay().iif([Activity(teacher=Teacher("prof1"), schedule=Schedule(day="Monday", beginning=8, end=12)), Activity(teacher=Teacher("prof1"), schedule=Schedule(day="Monday", beginning=12, end=14))])
        True
        >>> Teacher6HoursMaxPerDay().iif([Activity(teacher=Teacher("prof1"), schedule=Schedule(day="Monday", beginning=8, end=12)), Activity(teacher=Teacher("prof1"), schedule=Schedule(day="Tuesday", beginning=12, end=16))])
        True
        >>> Teacher6HoursMaxPerDay().iif([Activity(teacher=Teacher("prof1"), schedule=Schedule(day="Monday", beginning=8, end=12)), Activity(teacher=Teacher("prof1"), schedule=Schedule(day="Monday", beginning=12, end=16))])
        False
        """
        self.hours_day_teacher = {}
        for teacher in activities.get_teachers():
            self.hours_day_teacher[teacher] = {}
            for cycle in activities.get_cycles(teacher=teacher):
                self.hours_day_teacher[teacher][cycle] = {}
                for act in activities.get_activities(teacher=teacher, cycle=cycle):
                    if act.schedule.day in self.hours_day_teacher[teacher][cycle]:
                        self.hours_day_teacher[teacher][cycle][act.schedule.day] += act.schedule.length()
                    else:
                        self.hours_day_teacher[teacher][cycle][act.schedule.day] = act.schedule.length()
                    if any([x > 6 for x in self.hours_day_teacher[teacher][cycle].values()]):
                        return False
        return True
    
    def clean(self, activities, remaining):
        remove = []
        for act in remaining:
            if act.teacher in self.hours_day_teacher and act.cycle in self.hours_day_teacher[act.teacher] and act.schedule.day in self.hours_day_teacher[act.teacher][act.cycle] and self.hours_day_teacher[act.teacher][act.cycle][act.schedule.day] >= 6:
                remove.append(act)
        return remove