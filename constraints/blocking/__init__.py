from constraints import Constraint

class BlockingConstraint(Constraint):
    
    def _clean(self, activities, remaining):
        return self.clean(activities, remaining)
    
    def clean(self, activities, remaining):
        return []