from constraints.blocking import BlockingConstraint
from model import Schedule

class NotTooManyHoursWeekTeacher(BlockingConstraint):
    def iif(self, activities):
        self.hours_week_teacher = {}
        for teacher in activities.get_teachers():
            self.hours_week_teacher[teacher] = {}
            for cycle in activities.get_cycles(teacher=teacher): # Will break because of extended cycles
                activities_cycle_teacher = activities.get_activities(teacher=teacher, cycle=cycle)
                nb_hours = sum([act.schedule.length() for act in activities_cycle_teacher])
                if nb_hours > teacher.max_hours:
                    return False
                self.hours_week_teacher[teacher][cycle] = nb_hours
        return True
    
    def clean(self, activities, remaining):
        mmin =  min([schedule.length() for schedule in Schedule.get()])
        remove = []
        for act in remaining:
            if act.teacher in self.hours_week_teacher and act.cycle in self.hours_week_teacher[act.teacher] and self.hours_week_teacher[act.teacher][act.cycle] + mmin > act.teacher.max_hours:
                remove.append(act)
        return remove