from constraints.validation import ValidationConstraint
from model import Teacher
from model import Cycle

class RightNumberHoursTeacher(ValidationConstraint):
    def iif(self, activities):
        for teacher in Teacher.get():
            for cycle in Cycle.get(): # Will break because of extended cycles
                activities_cycle_teacher = activities.get_activities(teacher=teacher, cycle=cycle)
                nb_hours = sum([act.schedule.length() for act in activities_cycle_teacher])
                if nb_hours < teacher.min_hours or nb_hours > teacher.max_hours:
                    return False
        return True