from constraints.validation import ValidationConstraint
from model import Classe

# TODO To be added: exception in case of 1.5h per sport. 2 sports in cycle. Only for non-cham classes. 
class OneSportPerCycle(ValidationConstraint):
    def iif(self, activities):
        for classe in Classe.get():
            for cycle in classe.cycles:
                if not len(activities.get_activities(classe=classe, cycle=cycle)) == 1:
                    return False
        return True