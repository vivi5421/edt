from constraints.validation import ValidationConstraint
from model import Classe

class AtLeastOnePerCp(ValidationConstraint):
    def iif(self, activities):
        """
        >>> AtLeastOnePerCp().iif([Activity(classe=Classe(6, 1, None, None, None), sport=Sport("cp1", 1)), Activity(classe=Classe(6, 1, None, None, None), sport=Sport("cp2", 2)), Activity(classe=Classe(6, 1, None, None, None), sport=Sport("cp3", 3)), Activity(classe=Classe(6, 1, None, None, None), sport=Sport("cp4", 4))])
        True
        >>> AtLeastOnePerCp().iif([Activity(classe=Classe(6, 1, None, None, None), sport=Sport("cp1", 1)), Activity(classe=Classe(6, 1, None, None, None), sport=Sport("cp2", 2)), Activity(classe=Classe(6, 1, None, None, None), sport=Sport("cp3", 3))])
        False
        """
        for classe in Classe.get():
            sports = activities.get_sports(classe)
            cp1 = len([sport for sport in sports if sport.cp == 1])
            cp2 = len([sport for sport in sports if sport.cp == 2])
            cp3 = len([sport for sport in sports if sport.cp == 3])
            cp4 = len([sport for sport in sports if sport.cp == 4])
            if cp1 * cp2 * cp3 * cp4 == 0:
                return False
        return True