import logging

logger = logging.getLogger("edt")

class Constraint(object):

    def __init__(self, args=None):
        self.args = args

    # return constraint_result, filtered_remaining
    def _iif(self, activities):
        if not self.iif(activities):
            logger.debug("Failing because of: {}".format(self.__class__.__name__))
            return False
        return True
    
    def iif(self, activities):
        raise NotImplementedError(self) 