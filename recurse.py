import logging
import os
from concurrent.futures import ProcessPoolExecutor
from functools import partial
from groups import Groups
from utils import binom, str_list
from test_setup_exception import TestSetupException

logger = logging.getLogger("edt")

def increase_last(indices):
    return indices[:-1] + [indices[-1] + 1]


def add_one(indices):
    return indices + [indices[-1] + 1]


def valid_indices(indices, n):
    for i in reversed(range(len(indices))):
        if indices[i] >= n:
            if i == 0:
                return None
            del indices[i]
            indices[i-1] += 1
    return indices


def save_checkpoint(args, indices, curr, mmax):
    with open(args.checkpoint_file, "w") as f:
        f.write("{}\n{}\n{}".format(indices, curr, mmax))
    args.is_save_required = False
    if logger.isEnabledFor(logging.INFO):
        print()
    logger.info("Saving:\n\tindices: {}\n\tcurr: {}\n\tmax: {}".format(indices, curr, mmax))


def load_checkpoint(args):
    with open(args.checkpoint_file, "r") as f:
        checkpoint = f.readlines()
    indices = eval(checkpoint[0])
    curr = int(checkpoint[1])
    mmax = int(checkpoint[2])
    print()
    logger.info("Loading:\n\tindices: {}\n\tcurr: {}\n\tmax: {}".format(indices, curr, mmax))
    return indices, curr, mmax


def step(args, valid=False):
    if logger.isEnabledFor(logging.DEBUG):
        if valid and args.steps_when_valid or (not valid and args.steps): # We need the 'not valid' to do not repeat the step
            print()
            input("Press Enter to continue...")

def _recurse(args, pool, n, indices, r, validate_blocking, validate_full, progress):
    indices = valid_indices(indices, n)
    if indices:
        if args.is_save_required:
            save_checkpoint(args, indices, progress._current, progress._max)
        res = tuple(pool[i] for i in indices)
        if logger.isEnabledFor(logging.DEBUG):
            print()
            logger.debug("Checking for blocking filters...")
        groups = Groups(res)
        if validate_blocking(args=args, activities=res, indices=indices, groups=groups):
            if len(indices) == r:
                progress.next()
                if logger.isEnabledFor(logging.DEBUG):
                    print()
                    logger.debug("Checking for full filters...")
                if validate_full(args=args, activities=res, indices=indices, groups=groups):
                    if args.valids:
                        print()
                        logger.info("{} => Valid solution:\n{}".format(indices, str_list(res, rank=True)))
                    step(args, valid=True)
                    return increase_last(indices), res
                step(args, valid=True)
                return increase_last(indices), None
            else:
                step(args, valid=True)
                return add_one(indices), None
        else:
            if len(indices) == r:
                progress.next()
            progress.next(binom(r - len(indices), n - indices[-1] - 1))
            return increase_last(indices), None

def recurse(startpoint, args, iterable, r, validate_blocking, validate_full, progress):
    results = []
    pool = tuple(iterable)
    n = len(pool)
    if args.test_setup:
        mmethod = partial(_recurse, pool=pool, n=n, r=r, validate_blocking=validate_blocking, validate_full=validate_full, progress=progress)
        logger.warning("We are in test mode, returning partial method: {}".format(mmethod))
        raise TestSetupException(mmethod)
    if os.path.exists(args.checkpoint_file) and not args.restart:
        indices, curr, mmax = load_checkpoint(args)
        progress._max = mmax
        progress.next(curr)
        args.last_saved = curr
    else:
        indices = [startpoint]
    rs = [indices, None]
    while rs is not None:
        rs = _recurse(args, pool, n, indices, r, validate_blocking, validate_full, progress)
        if rs:
            indices = rs[0]
            if rs[1]:
                results.append(rs[1])
        step(args)
    return results
